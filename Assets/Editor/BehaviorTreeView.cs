using System;
using System.Collections.Generic;
using System.Linq;
using TheDayIDied.AI.BehaviourTrees;
using TheDayIDied.AI.BehaviourTrees.Actions;
using TheDayIDied.AI.BehaviourTrees.Composites;
using TheDayIDied.AI.BehaviourTrees.Decorators;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine.UIElements;

namespace TheDayIDied.Editor
{
    public class BehaviorTreeView : GraphView
    {
        public Action<NodeView> OnNodeSelected;
        private BehaviourTree _tree;
        
        public new class UxmlFactory : UxmlFactory<BehaviorTreeView, UxmlTraits>{}
        
        public BehaviorTreeView()
        {
            Insert(0, new GridBackground());

            this.AddManipulator(new ContentZoomer());
            this.AddManipulator(new ContentDragger());
            this.AddManipulator(new SelectionDragger());
            this.AddManipulator(new RectangleSelector());
            
            var styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>("Assets/Editor/BehaviorTreeEditor.uss");
            styleSheets.Add(styleSheet);

            Undo.undoRedoPerformed += OnUndoRedo;

        }

        private void OnUndoRedo()
        {
            PopulateView(_tree);
            AssetDatabase.SaveAssets();
        }
        
        public override void BuildContextualMenu(ContextualMenuPopulateEvent evt)
        {
            //base.BuildContextualMenu(evt);
            {
                var types = TypeCache.GetTypesDerivedFrom<ActionNode>();
                foreach (var type in types)
                {
                    evt.menu.AppendAction($"Actions/{type.Name}", (a) => CreateNode(type));
                }
            }
            {
                var types = TypeCache.GetTypesDerivedFrom<CompositeNode>();
                foreach (var type in types)
                {
                    evt.menu.AppendAction($"Composite Nodes/{type.Name}", (a) => CreateNode(type));
                }
            }
            {
                var types = TypeCache.GetTypesDerivedFrom<DecoratorNode>();
                foreach (var type in types)
                {
                    evt.menu.AppendAction($"Decorator Nodes/{type.Name}", (a) => CreateNode(type));
                }
            }
        }

        public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter)
        {
            //So ports dont connect to each other
            return ports.ToList().Where(endPort => endPort.direction != startPort.direction &&
                                                   endPort.node != startPort.node).ToList();
        }

       
        public NodeView FindNodeView(BehaviourNode behaviourNode)
        {
            return GetNodeByGuid(behaviourNode.guid) as NodeView;
        }
        
        public void PopulateView(BehaviourTree tree)
        {
            _tree = tree;

            graphViewChanged -= OnGraphViewChanged;
            DeleteElements(graphElements.ToList());
            graphViewChanged += OnGraphViewChanged;

            if (tree.rootNode == null)
            {
                tree.rootNode = tree.CreateNode(typeof(RootNode)) as RootNode;
                EditorUtility.SetDirty(tree);
                AssetDatabase.SaveAssets();
            }
            
            //Creates node View
            tree.nodes.ForEach(CreateNodeView);
            
            //Create Edges
            tree.nodes.ForEach(n =>
            {
                var children = tree.GetChildren(n);
                children.ForEach(c =>
                {
                    NodeView parentView = FindNodeView(n);
                    NodeView childView = FindNodeView(c);

                    Edge edge = parentView.output.ConnectTo(childView.input);
                    AddElement(edge);
                });
            });
        }

        private GraphViewChange OnGraphViewChanged(GraphViewChange graphviewchange)
        {
            if (graphviewchange.elementsToRemove != null)
            {
                graphviewchange.elementsToRemove.ForEach(e =>
                {
                    NodeView nodeView = e as NodeView;
                    if (nodeView != null)
                    {
                        _tree.DeleteNode(nodeView.BehaviourNode);
                    }

                    Edge edge = e as Edge;
                    if (edge != null)
                    {
                        NodeView parentView = edge.output.node as NodeView;
                        NodeView childView = edge.input.node as NodeView;
                        
                        _tree.RemoveChild(parentView.BehaviourNode, childView.BehaviourNode);
                    }
                });
            }

            if (graphviewchange.edgesToCreate != null)
            {
                graphviewchange.edgesToCreate.ForEach(edge =>
                {
                    NodeView parentView = edge.output.node as NodeView;
                    NodeView childView = edge.input.node as NodeView;
                    _tree.AddChild(parentView.BehaviourNode, childView.BehaviourNode);
                });
            }

            if (graphviewchange.movedElements != null)
            {
                nodes.ForEach((n) =>
                {
                    NodeView view = n as NodeView;
                    if (view != null) view.SortChildren();
                });
            }
            
            return graphviewchange;
        }
        void CreateNode(System.Type type)
        {
            BehaviourNode behaviourNode = _tree.CreateNode(type);
            CreateNodeView(behaviourNode);
        }

        public void CreateNodeView(BehaviourNode behaviourNode)
        {
            NodeView nodeView = new NodeView(behaviourNode);
            nodeView.OnNodeSelected = OnNodeSelected;
            AddElement(nodeView);
        }

        public void UpdateNodeState()
        {
            nodes.ForEach(n =>
            {
                NodeView view = n as NodeView;
                view.UpdateState();
            });
        }
    }
}
