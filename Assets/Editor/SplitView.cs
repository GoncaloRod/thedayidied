using UnityEngine.UIElements;

namespace TheDayIDied.Editor
{
    public class SplitView : TwoPaneSplitView
    {
        public new class UxmlFactory : UxmlFactory<SplitView, UxmlTraits>{}
    }
}
