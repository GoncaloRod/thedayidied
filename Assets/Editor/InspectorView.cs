using UnityEngine;
using UnityEngine.UIElements;


namespace TheDayIDied.Editor
{
    public class InspectorView : VisualElement
    {
        public new class UxmlFactory : UxmlFactory<InspectorView, UxmlTraits>{}

        private UnityEditor.Editor _editor;
        
        public InspectorView()
        {
            
        }

        public void UpdateSelection(NodeView nodeView)
        {
            Clear();
            
            UnityEngine.Object.DestroyImmediate(_editor);
            _editor = UnityEditor.Editor.CreateEditor(nodeView.BehaviourNode);
            
            IMGUIContainer container = new IMGUIContainer(() =>
            {
                if (_editor.target)
                    _editor.OnInspectorGUI();
                
            });
            Add(container);
        }
    }
}
