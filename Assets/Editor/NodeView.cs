using System;
using TheDayIDied.AI.BehaviourTrees;
using TheDayIDied.AI.BehaviourTrees.Actions;
using TheDayIDied.AI.BehaviourTrees.Composites;
using TheDayIDied.AI.BehaviourTrees.Decorators;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace TheDayIDied.Editor
{
    public class NodeView : UnityEditor.Experimental.GraphView.Node
    {
        public Action<NodeView> OnNodeSelected;
        public BehaviourNode BehaviourNode;
        public Port input;
        public Port output;
        
        
        public NodeView(BehaviourNode behaviourNode) : base("Assets/Editor/NodeView.uxml")
        {
            BehaviourNode = behaviourNode;
            title = behaviourNode.name;
            viewDataKey = behaviourNode.guid;

            style.left = behaviourNode.pos.x;
            style.top = behaviourNode.pos.y;

            CreateInputPorts();
            CreateOutputPorts();

            SetupNodes();
        }

        private void SetupNodes()
        {
            switch (BehaviourNode)
            {
                case ActionNode _:
                    AddToClassList("action");
                    break;
                case CompositeNode _:
                    AddToClassList("composite");
                    break;
                case RootNode _:
                    AddToClassList("root");
                    break;
                case DecoratorNode _:
                    AddToClassList("decorator");
                    break;
            }
        }

        private void CreateInputPorts()
        {
            if (BehaviourNode is ActionNode)
                input = InstantiatePort(Orientation.Vertical, Direction.Input, Port.Capacity.Single, typeof(bool));
            else if (BehaviourNode is CompositeNode)
                input = InstantiatePort(Orientation.Vertical, Direction.Input, Port.Capacity.Single, typeof(bool));
            else if (BehaviourNode is RootNode) {}
            else if (BehaviourNode is DecoratorNode)
                input = InstantiatePort(Orientation.Vertical, Direction.Input, Port.Capacity.Single, typeof(bool));

            if (input != null)
            {
                input.portName = " ";
                input.style.flexDirection = FlexDirection.Column;
                inputContainer.Add(input);
            }
        }

        private void CreateOutputPorts()
        {
            if (BehaviourNode is ActionNode) { } 
            else if (BehaviourNode is CompositeNode)
                output = InstantiatePort(Orientation.Vertical, Direction.Output, Port.Capacity.Multi, typeof(bool));
            else if (BehaviourNode is RootNode)
                output = InstantiatePort(Orientation.Vertical, Direction.Output, Port.Capacity.Single, typeof(bool));
            else if (BehaviourNode is DecoratorNode)
                output = InstantiatePort(Orientation.Vertical, Direction.Output, Port.Capacity.Single, typeof(bool));

            if (output != null)
            {
                output.portName = "";
                output.style.flexDirection = FlexDirection.ColumnReverse;
                outputContainer.Add(output);
            }
        }

        public override void SetPosition(Rect newPos)
        {
            base.SetPosition(newPos);
            
            Undo.RecordObject(BehaviourNode, "Behaviour Tree (Set Position)");
            
            BehaviourNode.pos.x = newPos.xMin;
            BehaviourNode.pos.y = newPos.yMin;
            
            EditorUtility.SetDirty(BehaviourNode);
        }

        public override void OnSelected()
        {
            base.OnSelected();
            if (OnNodeSelected != null)
            {
                OnNodeSelected.Invoke(this);
            }
        }

        public void SortChildren()
        {
            CompositeNode composite = BehaviourNode as CompositeNode;

            if (composite)
            {
                composite.children.Sort(SortByHorizontalPos);
            }
        }

        private int SortByHorizontalPos(BehaviourNode left, BehaviourNode right)
        {
            return left.pos.x < right.pos.x ? -1 : 1;
        }

        public void UpdateState()
        {
            RemoveFromClassList("running");
            RemoveFromClassList("success");
            RemoveFromClassList("failure");
            
            if (Application.isPlaying)
            {
                switch (BehaviourNode.NodeState)
                {
                    case NodeState.Running:
                        if (BehaviourNode.started)
                        {
                            AddToClassList("running");
                        }
                        break;
                    case NodeState.Success:
                        AddToClassList("success");
                        break;
                    case NodeState.Failure:
                        AddToClassList("failure");
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }      
            }
            
           
        }
    }
}
