using System.Collections.Generic;
using TheDayIDied.Mono.NPC;
using UnityEngine;

namespace TheDayIDied.Mono.Player
{
    [RequireComponent(typeof(PlayerInput))]
    public class PlayerMoodAbility : MonoBehaviour
    {
        private PlayerInput _playerInput;
        
        public static List<Mood> MoodVFXSystems = new List<Mood>();
        
        private void Awake()
        {
            _playerInput = GetComponent<PlayerInput>();
            
            _playerInput.ActivateMood += ActivateMood;
        }

        private void ActivateMood()
        {
            foreach (var moodVFXSystem in MoodVFXSystems)
            {
                moodVFXSystem.ToggleMood();
            }
        }
    }
}
