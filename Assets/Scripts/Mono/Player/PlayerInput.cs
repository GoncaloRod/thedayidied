﻿using System;
using TheDayIDied.Mono.UI;
using UnityEngine;

namespace TheDayIDied.Mono.Player
{
    public class PlayerInput : MonoBehaviour
    {
        private InputActions _actions;

        private ClueInspector _clueInspector;
        
        public Vector2 Movement { get; private set; }
        
        public Vector2 Look { get; private set; }
        
        public float VerticalMovement { get; private set; }
        
        public bool Running { get; private set; }
        
        public Action Interact { get; set; }
        
        public Action NextInteraction { get; set; }
        
        public Action PreviousInteraction { get; set; }
        
        public Action ActivateMood { get; set; }
        
        public Action PickUpObject { get; set; }
        
        public Action DropObject { get; set; }

        private void Awake()
        {
            _actions = new InputActions();

            _actions.Player.Run.performed += ctx => Running = true;
            _actions.Player.Run.canceled += ctx => Running = false;

            _actions.Player.Interact.performed += ctx => Interact();
            
            _actions.Player.NextInteraction.performed += ctx => NextInteraction();
            _actions.Player.PreviousInteraction.performed += ctx => PreviousInteraction();
            
            _actions.Player.ActivateMood.performed += ctx => ActivateMood();

            _actions.Player.Levitate.performed += ctx => PickUpObject();
            _actions.Player.Levitate.canceled += ctx => DropObject();
        }

        private void Start()
        {
            _clueInspector = ClueInspector.Instance;
            
            if (_clueInspector != null)
            {
                ClueInspector.Instance.OnClueInspectorShow += () => enabled = false;
                ClueInspector.Instance.OnClueInspectorHide += () => enabled = true;
            }
        }

        private void Update()
        {
            Movement = _actions.Player.Movement.ReadValue<Vector2>();
            Look = _actions.Player.Look.ReadValue<Vector2>();
            VerticalMovement = _actions.Player.VerticalMovement.ReadValue<float>();
            
            float changeInteractionAxis = _actions.Player.ChangeInteractionAxis.ReadValue<float>();
            
            if (changeInteractionAxis > 0)
                PreviousInteraction();
            else if (changeInteractionAxis < 0)
                NextInteraction();
        }

        private void OnEnable()
        {
            _actions.Enable();
        }

        private void OnDisable()
        {
            _actions.Disable();
        }
    }
}