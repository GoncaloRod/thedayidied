using System;
using TheDayIDied.ScriptableObjects;
using UnityEngine;

namespace TheDayIDied.Mono.Player
{
    public class PlayerInventory : MonoBehaviour
    {
        [SerializeField] private ClueInventory clueInventory;

        public ClueInventory ClueInventory => clueInventory;
    }
}
