using System;
using UnityEngine;

namespace TheDayIDied.Mono.Player
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(PlayerInput))]
    public class PlayerController : MonoBehaviour
    {
        private PlayerInput _input;
        
        private Rigidbody _rigidbody;

        private Vector3 _localRotation = Vector3.zero;

        [SerializeField] private float walkSpeed = 1f;
        [SerializeField] private float runSpeed = 1f;
        [SerializeField] private float sensitivity = 1f;

        private void Awake()
        {
            _input = GetComponent<PlayerInput>();
            
            _rigidbody = GetComponent<Rigidbody>();

            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

        private void Update()
        {
            Vector2 moventInput = _input.Movement;
            Vector2 mouseInput = _input.Look;
            float verticalMovementInput = _input.VerticalMovement;

            float speed = _input.Running ? runSpeed : walkSpeed;
            
            if (moventInput.x != 0f || moventInput.y != 0f || verticalMovementInput != 0f)
                _rigidbody.velocity = transform.forward * (moventInput.y * speed) +  transform.right * (moventInput.x * speed) + Vector3.up * (verticalMovementInput * speed);

            _localRotation.x += -mouseInput.y * sensitivity * Time.deltaTime;
            _localRotation.y += mouseInput.x * sensitivity * Time.deltaTime;

            if (_localRotation.x > 90f)
                _localRotation.x = 90f;
            else if (_localRotation.x < -90f)
                _localRotation.x = -90f;

            transform.localEulerAngles = _localRotation;
        }
    }
}
