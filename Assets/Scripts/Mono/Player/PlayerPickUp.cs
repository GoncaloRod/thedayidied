using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace TheDayIDied.Mono.Player
{
    [RequireComponent(typeof(PlayerInput))]
    public class PlayerPickUp : MonoBehaviour
    {
        private PlayerInput _input;
        
        private GameObject _levitatingObj;
        private Transform _levitateParentDefaultPos;
        private Vector3 _randomDegrees;
        private LayerMask _defaultLayer;

        [SerializeField] private float pickUpRange = 4.0f;
        [SerializeField] private float moveForce = 250.0f;
        [SerializeField] private Transform levitateParent;
        [SerializeField] private LayerMask layer;
        [SerializeField] [Range(1,200)] private int drag = 20;

        private void Awake()
        {
            _input = GetComponent<PlayerInput>();
            
            _input.PickUpObject += PickUpObject;
            _input.DropObject += DropObject;
        }

        private void Start()
        {
            _levitateParentDefaultPos = levitateParent;
        }

        // Update is called once per frame
        void Update()
        {
            if (_levitatingObj != null)
            {
                MoveObject();
                RotateObject();
            }
        }

        private void PickUpObject()
        {
            //Debug.DrawRay();
            RaycastHit hit;

            GameObject targetObject;
            
            if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, pickUpRange))
            { 
                if (hit.rigidbody != null)
                {
                    targetObject = hit.transform.gameObject;
                    
                    levitateParent.position = targetObject.transform.position;
                    _defaultLayer = targetObject.layer;
                    
                    targetObject.layer = LayerMask.NameToLayer("Levitating");
                    
                    LevitateObject(hit.transform.gameObject); 
                }
            }
        }
        
        private void LevitateObject(GameObject levitateObj)
        {
            _randomDegrees.x = Random.Range(-180, 180);
            _randomDegrees.y = Random.Range(-180, 180);
            _randomDegrees.z = Random.Range(-180, 180);
            
            _randomDegrees.Normalize();
            
            if (levitateObj.GetComponent<Rigidbody>())
            {
                Rigidbody objRb = levitateObj.GetComponent<Rigidbody>();
                objRb.useGravity = false;
                objRb.drag = drag;

                _levitatingObj = levitateObj;
            }
        }
        
        private void DropObject()
        {
            if (_levitatingObj != null)
            {
                Rigidbody rb = _levitatingObj.GetComponent<Rigidbody>();
                rb.useGravity = true;
                rb.drag = 1;

                _levitatingObj.layer = _defaultLayer.value;
                _levitatingObj.transform.parent = null;
                _levitatingObj = null;
                levitateParent = _levitateParentDefaultPos;
            }
        }

        private void MoveObject()
        {
            if (Vector3.Distance(_levitatingObj.transform.position, levitateParent.position) > 0.1f)
            {
                Vector3 moveDirection = levitateParent.position - _levitatingObj.transform.position;
                _levitatingObj.GetComponent<Rigidbody>().AddForce(moveDirection * moveForce);
            }
        }

        private void RotateObject()
        {
            _levitatingObj.transform.Rotate(_randomDegrees * 0.2f);
        }
    }
}
