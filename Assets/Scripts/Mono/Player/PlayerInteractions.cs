using TheDayIDied.Mono.Interactables;
using TheDayIDied.Mono.Interactables.Clues;
using TheDayIDied.Mono.Player;
using UnityEngine;

namespace TheDayIDied
{
    [RequireComponent(typeof(PlayerInput))]
    [RequireComponent(typeof(PlayerInventory))]
    public class PlayerInteractions : MonoBehaviour
    {
        private PlayerInput _input;

        private PlayerInventory _inventory;
        
        private Transform _cameraTransform;

        private Interactable _interactable;
        private Interactable _interactableLastFrame;

        [SerializeField] private float maxInteractionDistance = 1f;

        private void Awake()
        {
            _input = GetComponent<PlayerInput>();

            _inventory = GetComponent<PlayerInventory>();
            
            _input.Interact += Interact;
            _input.NextInteraction += NextInteraction;
            _input.PreviousInteraction += PreviousInteraction;

            _cameraTransform = Camera.main.transform;
        }

        private void Update()
        {
            CheckInteractableInFront();
        }

        private void CheckInteractableInFront()
        {
            _interactableLastFrame = _interactable;
            
            RaycastHit hit;
            
            if (Physics.Raycast(_cameraTransform.position, _cameraTransform.forward, out hit, maxInteractionDistance))
            {
                _interactable = hit.collider.GetComponent<Interactable>();
            }
            else
            {
                _interactable = null;
            }

            if (_interactableLastFrame == null && _interactable != null)
            {
                OnInteractableEnter();
            }
            else if (_interactableLastFrame != null && _interactable == null)
            {
                OnInteractableExit();
            }
        }

        private void OnInteractableEnter()
        {
            _interactable.ShowInteractionDisplay();
        }
        
        private void OnInteractableExit()
        {
            _interactableLastFrame.HideInteractionDisplay();
        }

        private void NextInteraction()
        {
            if (_interactable == null) return;

            _interactable.CurrentInteractionIndex++;
        }

        private void PreviousInteraction()
        {
            if (_interactable == null) return;
            
            _interactable.CurrentInteractionIndex--;
        }

        private void Interact()
        {
            if (_interactable == null) return;
            
            _interactable.Interact();

            if (_interactable is Clue clue)
            {
                _inventory.ClueInventory.Add(clue.ClueData);
            }
        }
    }
}
