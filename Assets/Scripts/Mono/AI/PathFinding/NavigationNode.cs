﻿using System.Collections.Generic;
using UnityEngine;

namespace TheDayIDied.Mono.AI.PathFinding
{
    public abstract class NavigationNode
    {
        public abstract Vector3 Position { get; }
        
        public abstract bool Occupied { get; }

        public List<NavigationNodeConnection> Connections { get; private set; }

        protected NavigationNode()
        {
            Connections = new List<NavigationNodeConnection>();
        }

        public void AddConnection(NavigationNodeConnection connection)
        {
            Connections.Add(connection);
        }
    }
}