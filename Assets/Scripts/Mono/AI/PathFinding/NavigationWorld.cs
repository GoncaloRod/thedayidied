﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace TheDayIDied.Mono.AI.PathFinding
{
    public class NavigationWorld : NavigationGraph<NavigationWorldNode>
    {
        public static List<NavigationAgent> Agents = new List<NavigationAgent>();
        public static List<NavigationAreaLink> Links = new List<NavigationAreaLink>();
        
        private static int _trackedAgentCount = 0;

        private void Start()
        {
            GenerateNodes();
            
            AssignAgents();
        }

        private void Update()
        {
            if (_trackedAgentCount != Agents.Count)
            {
                AssignAgents();
            }
        }

        private void GenerateNodes()
        {
            List<NavigationWorldNode> nodes = new List<NavigationWorldNode>();

            foreach (NavigationAreaLink link in Links)
            {
                NavigationWorldNode fromNode = nodes.FirstOrDefault(node => node.Area == link.From);
                NavigationWorldNode toNode = nodes.FirstOrDefault(node => node.Area == link.To);

                if (fromNode == null)
                {
                    fromNode = new NavigationWorldNode(link.From.transform.position, link.From);
                    link.From.AssignedWorld = this;
                    nodes.Add(fromNode);
                }
                
                if (toNode == null)
                {
                    toNode = new NavigationWorldNode(link.To.transform.position, link.To);
                    link.To.AssignedWorld = this;
                    nodes.Add(toNode);
                }
                
                fromNode.AddConnection(new NavigationWorldConnection(
                    fromNode,
                    link.AreaFromNode,
                    toNode,
                    link.AreaToNode,
                    link,
                    (toNode.Position - fromNode.Position).magnitude)
                );
                
                if (link.Bidirectional)
                    toNode.AddConnection(new NavigationWorldConnection(
                        toNode,
                        link.AreaToNode,
                        fromNode,
                        link.AreaFromNode,
                        link,
                        (fromNode.Position - toNode.Position).magnitude)
                    );
            }

            Nodes = nodes.ToArray();
        }

        private void AssignAgents()
        {
            foreach (NavigationAgent agent in Agents)
            {
                agent.World = this;
            }

            _trackedAgentCount = Agents.Count;
        }

        public List<NavigationNodeConnection> RequestPath(Vector3 from, Vector3 to)   // TODO: Path class to return
        {
            // TODO: Job
            List<NavigationNodeConnection> worldPath = FindPath(from, to);
            
            if (worldPath == null) return null;

            NavigationAreaNode startNode;
            NavigationAreaNode endNode;
            
            startNode = WorldPositionToNode(from).Area.WorldPositionToNode(from);
            endNode = WorldPositionToNode(to).Area.WorldPositionToNode(to);

            Queue<NavigationAreaNode> checkpoints = new Queue<NavigationAreaNode>();
            
            checkpoints.Enqueue(startNode);

            foreach (var connection in worldPath.Cast<NavigationWorldConnection>())
            {
                checkpoints.Enqueue(connection.AreaNodeFrom);
                checkpoints.Enqueue(connection.AreaNodeTo);
            }
            
            checkpoints.Enqueue(endNode);

            List<(Vector3 from, Vector3 to, NavigationArea area)> pathFindingTasks = new List<(Vector3, Vector3, NavigationArea)>();

            while (checkpoints.Count > 0)
            {
                NavigationNode f = checkpoints.Dequeue();
                NavigationNode t = checkpoints.Dequeue();
                NavigationArea a = WorldPositionToNode(f.Position).Area;
                
                pathFindingTasks.Add((f.Position, t.Position, a));
            }

            List<NavigationNodeConnection> finalPath = new List<NavigationNodeConnection>();
            foreach (var task in pathFindingTasks)
            {
                finalPath.AddRange(task.area.FindPath(task.from, task.to));
            }

            return finalPath;
        }
        
        public override NavigationWorldNode WorldPositionToNode(Vector3 position)
        {
            foreach (NavigationWorldNode node in Nodes)
                if (node.Area.IsPointInsideArea(position))
                    return node;

            return null;
        }
        
        private void OnDestroy()
        {
            Agents.Clear();
            Links.Clear();
        }

        private void OnDrawGizmosSelected()
        {
            if (!Application.isPlaying) return;
            
            foreach (NavigationNode node in Nodes)
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawSphere(node.Position, 0.2f);

                Gizmos.color = Color.yellow;
                foreach (NavigationNodeConnection connection in node.Connections)
                {
                    Gizmos.DrawLine(connection.From.Position, connection.To.Position);
                }
            }
        }
    }
}