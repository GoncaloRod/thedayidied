﻿using UnityEngine;

namespace TheDayIDied.Mono.AI.PathFinding.Heuristics
{
    public class ManhattanDistanceHeuristic : IHeuristic
    {
        public float Estimate(NavigationNode from, NavigationNode to)
        {
            Vector3 dir = to.Position - from.Position;

            return Mathf.Abs(dir.x + dir.y + dir.z);
        }
    }
}