﻿namespace TheDayIDied.Mono.AI.PathFinding.Heuristics
{
    public interface IHeuristic
    {
        public float Estimate(NavigationNode from, NavigationNode to);
    }
}