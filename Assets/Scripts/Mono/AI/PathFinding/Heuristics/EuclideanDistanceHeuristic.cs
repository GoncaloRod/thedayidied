﻿namespace TheDayIDied.Mono.AI.PathFinding.Heuristics
{
    public class EuclideanDistanceHeuristic : IHeuristic
    {
        public float Estimate(NavigationNode from, NavigationNode to)
        {
            return (to.Position - from.Position).magnitude;
        }
    }
}