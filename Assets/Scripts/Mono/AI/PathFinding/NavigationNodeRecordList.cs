﻿using System.Collections.Generic;

namespace TheDayIDied.Mono.AI.PathFinding
{
    public class NavigationNodeRecordList<T> : List<NavigationNodeRecord<T>> where T : NavigationNode
    {
        public NavigationNodeRecord<T> Cheapest => Count > 0 ? this[0] : null;

        public NavigationNodeRecord<T> Find(NavigationNode node)
        {
            foreach (NavigationNodeRecord<T> record in this)
                if (record.Node == node)
                    return record;
            
            return null;
        }

        public bool Contains(NavigationNode node)
        {
            return Find(node) != null;
        }

        public new void Sort()
        {
            Sort((a, b) =>
            {
                float dif = a.TotalCost - b.TotalCost;

                if (dif < 0f) return -1;
                if (dif > 0f) return 1;

                // If both nodes have the same f cost, choose the one closest to target
                dif = a.HeuristicCost - b.HeuristicCost;
                
                if (dif < 0f) return -1;
                if (dif > 0f) return 1;
                return 0;
            });
        }
    }
}