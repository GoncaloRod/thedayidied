﻿using UnityEngine;

namespace TheDayIDied.Mono.AI.PathFinding
{
    public abstract class NavigationArea : NavigationGraph<NavigationAreaNode>
    {
        [HideInInspector]
        public NavigationWorld AssignedWorld = null;

        public abstract bool IsPointInsideArea(Vector3 position);
    }
}