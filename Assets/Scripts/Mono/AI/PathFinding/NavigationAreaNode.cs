﻿using UnityEngine;

namespace TheDayIDied.Mono.AI.PathFinding
{
    public class NavigationAreaNode : NavigationNode
    {
        public override Vector3 Position { get; }
        
        public override bool Occupied { get; }

        public NavigationAreaNode(Vector3 position, bool occupied = false)
        {
            Position = position;
            Occupied = occupied;
        }
    }
}