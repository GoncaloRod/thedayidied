﻿namespace TheDayIDied.Mono.AI.PathFinding
{
    public class NavigationWorldConnection : NavigationNodeConnection
    {
        public override NavigationNode From { get; }

        public NavigationAreaNode AreaNodeFrom { get; private set; }
        
        public override NavigationNode To { get; }
        
        public NavigationAreaNode AreaNodeTo { get; private set; }
        
        public override float Cost { get; }
        
        public NavigationAreaLink Link { get; private set; }

        public NavigationWorldConnection(NavigationNode from, NavigationAreaNode areaNodeFrom, NavigationNode to, NavigationAreaNode areaNodeTo, NavigationAreaLink link, float cost)
        {
            From = from;
            AreaNodeFrom = areaNodeFrom;
            To = to;
            AreaNodeTo = areaNodeTo;
            Cost = cost;
            Link = link;
        }
    }
}