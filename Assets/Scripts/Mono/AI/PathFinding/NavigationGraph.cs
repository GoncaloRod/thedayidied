﻿using System.Collections.Generic;
using TheDayIDied.Mono.AI.PathFinding.Heuristics;
using UnityEngine;

namespace TheDayIDied.Mono.AI.PathFinding
{
    public abstract class NavigationGraph<T> : MonoBehaviour where T : NavigationNode
    {
        protected static IHeuristic Heuristic = new EuclideanDistanceHeuristic();

        protected T[] Nodes;

        public abstract T WorldPositionToNode(Vector3 position);
        
        public List<NavigationNodeConnection> FindPath(Vector3 from, Vector3 to)
        {
            T start = WorldPositionToNode(from);
            T target = WorldPositionToNode(to);

            if (start == null || target == null) return null;
            
            NavigationNodeRecord<T> current = null;
            NavigationNodeRecord<T> startRecord = new NavigationNodeRecord<T>(start, Heuristic.Estimate(start, target));

            NavigationNodeRecordList<T> open = new NavigationNodeRecordList<T>();
            NavigationNodeRecordList<T> closed = new NavigationNodeRecordList<T>();
            
            open.Add(startRecord);

            while (open.Count > 0)
            {
                current = open.Cheapest;

                open.Remove(current);
                closed.Add(current);
                
                // Found path
                if (current.Node == target)
                    break;

                foreach (NavigationNodeConnection connection in current.Node.Connections)
                {
                    T node = (T)connection.To;
                    
                    if (node.Occupied || closed.Contains(node))
                        continue;

                    NavigationNodeRecord<T> record = open.Find(node);
                    bool wasOpen = record != null;
                    
                    float cost = current.Cost + connection.Cost;

                    if (record == null || cost < record.Cost)
                    {
                        if (record == null)
                        {
                            record = new NavigationNodeRecord<T>(node, Heuristic.Estimate(node, target));
                        }

                        record.Cost = cost;
                        record.Connection = connection;
                        
                        if (!wasOpen)
                            open.Add(record);
                    }
                }
            }

            if (current.Node != target)
            {
                return null;
            }

            List<NavigationNodeConnection> path = new List<NavigationNodeConnection>();

            while (current.Node != start)
            {
                path.Add(current.Connection);
                current = closed.Find(current.Connection.From);
            }
            
            //path.Add(start);
            path.Reverse();

            return path;
        }
    }
}