﻿using System.Linq;
using TMPro;
using UnityEditor;
using UnityEngine;

namespace TheDayIDied.Mono.AI.PathFinding
{
    public class NavigationGrid : NavigationArea
    {
        private const int PartialBlockageSubdivisions = 2;
        private const float MaxHeightOffset = 0.1f;

        private Vector2 _cellSize;
        private Vector3 _bottomLeft;
        
        [SerializeField] private Vector2 realSize = Vector2.one;
        [SerializeField] private Vector2Int gridSize = Vector2Int.one;

        [Space]
        
        [SerializeField] private LayerMask unwalkableLayer;

        private void Awake()
        {
            _cellSize = new Vector2(realSize.x / gridSize.x, realSize.y / gridSize.y);
            _bottomLeft = transform.position - (transform.right * ((realSize.x - _cellSize.x) / 2f)) - (transform.forward * ((realSize.y - _cellSize.y) / 2f));
            
            CreateNodes();
            
            AddConnections();
        }

        private void CreateNodes()
        {
            Nodes = new NavigationAreaNode[gridSize.x * gridSize.y];
            
            for (int y = 0; y < gridSize.y; y++)
            {
                for (int x = 0; x < gridSize.x; x++)
                {
                    Vector3 pos = _bottomLeft + transform.right * (_cellSize.x * x) + transform.forward * (_cellSize.y * y);
                    
                    bool occupied = CheckOccupation(pos);
                    
                    Nodes[gridSize.x * y + x] = new NavigationAreaNode(pos, occupied);
                }
            }
        }

        private void AddConnections()
        {
            (int x, int y)[] neighbours = 
            {
                (-1, -1), (0, -1), (1, -1),
                (-1, 0), (1, 0),
                (-1, 1), (0, 1), (1, 1)
            };

            for (int y = 0; y < gridSize.y; y++)
            {
                for (int x = 0; x < gridSize.x; x++)
                {
                    NavigationAreaNode node = Nodes[gridSize.x * y + x];

                    foreach ((int x, int y) neighbour in neighbours)
                    {
                        (int x, int y) pos = (x + neighbour.x, y + neighbour.y);

                        if (pos.x >= 0 && pos.x < gridSize.x && pos.y >= 0 && pos.y < gridSize.y)
                        {
                            NavigationAreaNode neighbourNode = Nodes[gridSize.x * (y + neighbour.y) + (x + neighbour.x)];
                            float cost = (neighbourNode.Position - node.Position).magnitude;
                            
                            node.AddConnection(new NavigationAreaConnection(node, neighbourNode, cost));
                        }
                    }
                }
            }
        }

        private bool CheckOccupation(Vector3 cellPosition)
        {
            bool occupied = Physics.CheckBox(
                cellPosition + Vector3.up * 0.5f,
                new Vector3(_cellSize.x / 2f, 1f, _cellSize.y / 2f),
                transform.rotation,
                unwalkableLayer
            );
            
            if (occupied)
                occupied = CheckPartialBlockage(cellPosition);

            return occupied;
        }

        private bool CheckPartialBlockage(Vector3 cellPosition)
        {
            Vector3 cellBottomLeft = cellPosition - transform.right * (_cellSize.x / 2f) - transform.forward * (_cellSize.y / 2f);
            Vector2 subCellSize = _cellSize / PartialBlockageSubdivisions;
            Vector3 firstSubCellPos = cellBottomLeft + transform.right * (subCellSize.x / 2f) + transform.forward * (subCellSize.y / 2f);

            bool[] subCellOccupation = new bool[PartialBlockageSubdivisions * 2];

            for (int y = 0; y < PartialBlockageSubdivisions; y++)
            {
                for (int x = 0; x < PartialBlockageSubdivisions; x++)
                {
                    Vector3 subCellPos = firstSubCellPos +
                                         transform.right * subCellSize.x * x +
                                         transform.forward * subCellSize.y * y;
                                
                    subCellOccupation[PartialBlockageSubdivisions * y + x] = Physics.CheckBox(
                        subCellPos + Vector3.up * 0.5f,
                        new Vector3(subCellSize.x / 2f, 1f, subCellSize.y / 2f),
                        transform.rotation,
                        unwalkableLayer
                    );
                }
            }

            return subCellOccupation.Count(b => b) > PartialBlockageSubdivisions;
        }

        public override NavigationAreaNode WorldPositionToNode(Vector3 position)
        {
            Vector3 localPosition = transform.InverseTransformPoint(position) - transform.InverseTransformPoint(_bottomLeft);
            
            Vector2Int gridPos = new Vector2Int(
                (int)((localPosition.x + _cellSize.x / 2f) / _cellSize.x),
                (int)((localPosition.z + _cellSize.y / 2f) / _cellSize.y)
            );

            if (localPosition.y > MaxHeightOffset) return null;
            
            if (gridPos.x < 0 || gridPos.x > gridSize.x - 1) return null;
            if (gridPos.y < 0 || gridPos.y > gridSize.y - 1) return null;

            return Nodes[gridSize.x * gridPos.y + gridPos.x];
        }

        public override bool IsPointInsideArea(Vector3 position)
        {
            Vector3 localPosition = transform.InverseTransformPoint(position) - transform.InverseTransformPoint(_bottomLeft);
            
            Vector2Int gridPos = new Vector2Int(
                (int)((localPosition.x + _cellSize.x / 2f) / _cellSize.x),
                (int)((localPosition.z + _cellSize.y / 2f) / _cellSize.y)
            );

            if (localPosition.y > MaxHeightOffset) return false;
            
            if (gridPos.x < 0 || gridPos.x > gridSize.x - 1) return false;
            if (gridPos.y < 0 || gridPos.y > gridSize.y - 1) return false;

            return true;
        }

        private void OnDrawGizmosSelected()
        {
            if (!Application.isPlaying)
            {
                // Draw a preview of node positions
                Vector2 cellSize = new Vector2(realSize.x / gridSize.x, realSize.y / gridSize.y);
                Vector3 bottomLeft = transform.position - (transform.right * ((realSize.x - cellSize.x) / 2f)) - (transform.forward * ((realSize.y - cellSize.y) / 2f));
                
                Gizmos.DrawSphere(bottomLeft, 0.1f);

                for (int y = 0; y < gridSize.y; y++)
                {
                    for (int x = 0; x < gridSize.x; x++)
                    {
                        Gizmos.color = Color.white;
                        Vector3 pos = bottomLeft + transform.right * (cellSize.x * x) + transform.forward * (cellSize.y * y);
                        
                        Gizmos.DrawSphere(pos, 0.1f);
                        
                        Vector3 cellBottomLeft = pos - transform.right * (cellSize.x / 2f) - transform.forward * (cellSize.y / 2f);

                        Vector2 subCellSize = cellSize / PartialBlockageSubdivisions;

                        Vector3 firstSubCellPos = cellBottomLeft + transform.right * (subCellSize.x / 2f) + transform.forward * (subCellSize.y / 2f);

                        Gizmos.color = Color.green;
                        for (int sy = 0; sy < PartialBlockageSubdivisions; sy++)
                        {
                            for (int sx = 0; sx < PartialBlockageSubdivisions; sx++)
                            {
                                Gizmos.DrawSphere(
                                    firstSubCellPos + transform.right * subCellSize.x * sx + transform.forward * subCellSize.y * sy,
                                    0.04f
                                );
                            }
                        }
                    }
                }
            }
            else
            {
                // Draw accurate node positions
                foreach (NavigationNode node in Nodes)
                {
                    Gizmos.color = node.Occupied ? Color.red : Color.blue;
                    Gizmos.DrawSphere(node.Position, 0.1f);

                    Gizmos.color = Color.yellow;
                    foreach (NavigationNodeConnection connection in node.Connections)
                    {
                        Gizmos.DrawLine(connection.From.Position, connection.To.Position);
                    }
                }
            }
        }
    }
}