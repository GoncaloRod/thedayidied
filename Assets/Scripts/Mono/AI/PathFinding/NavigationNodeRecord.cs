﻿namespace TheDayIDied.Mono.AI.PathFinding
{
    public class NavigationNodeRecord<T> where T : NavigationNode
    {
        public T Node { get; private set; }
        
        public NavigationNodeConnection Connection { get; set; }
        
        public float Cost { get; set; }
        
        public float HeuristicCost { get; private set; }

        public float TotalCost => Cost + HeuristicCost;

        public NavigationNodeRecord(T node, float heuristicCost, NavigationNodeConnection connection = null, float cost = 0f)
        {
            Node = node;
            Connection = connection;
            Cost = cost;
            HeuristicCost = heuristicCost;
        }
    }
}