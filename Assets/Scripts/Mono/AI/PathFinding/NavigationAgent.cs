﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace TheDayIDied.Mono.AI.PathFinding
{
    public class NavigationAgent : MonoBehaviour
    {
        // TODO: Replace with path class
        private List<NavigationNodeConnection> _currentPath;
        private NavigationNodeConnection _currentTarget;

        public NavigationWorld World { get; set; }

        private void Update()
        {
            if (_currentTarget == null && _currentPath != null)
            {
                // New path available
                _currentTarget = _currentPath[0];
            }
            
            
        }

        // TODO: Replace with path class
        public bool CalculatePath(Vector3 target, out List<NavigationNodeConnection> path)
        {
            path = World.RequestPath(transform.position, target);

            return path != null;
        }

        public bool SetDestination(Vector3 target)
        {
            List<NavigationNodeConnection> path;
            
            if (CalculatePath(target, out path))
            {
                _currentPath = path;
                return true;
            }

            return false;
        }

        public bool SetPath()
        {
            throw new NotImplementedException();
        }
        
        private void OnEnable()
        {
            NavigationWorld.Agents.Add(this);
        }

        private void OnDisable()
        {
            NavigationWorld.Agents.Remove(this);
        }
        
        private void OnDrawGizmos()
        {
            if (_currentPath == null) return;

            Gizmos.color = Color.green;
            for (int i = 0; i < _currentPath.Count; i++)
            {
                Gizmos.DrawLine(_currentPath[i].From.Position, _currentPath[i].To.Position);
            }
        }
    }
}