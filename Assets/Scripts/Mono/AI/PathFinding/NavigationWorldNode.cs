﻿using UnityEngine;

namespace TheDayIDied.Mono.AI.PathFinding
{
    public class NavigationWorldNode : NavigationNode
    {
        public override Vector3 Position { get; }

        public override bool Occupied => false;

        public NavigationArea Area { get; private set; }
            
        public NavigationWorldNode(Vector3 position, NavigationArea area)
        {
            Position = position;
            Area = area;
        }
    }
}