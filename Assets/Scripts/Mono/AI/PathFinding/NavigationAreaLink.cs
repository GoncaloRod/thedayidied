﻿using System;
using UnityEngine;

namespace TheDayIDied.Mono.AI.PathFinding
{
    public class NavigationAreaLink : MonoBehaviour
    {
        private NavigationAreaNode _areaFromNode;
        private NavigationAreaNode _areaToNode;
        
        [Space]
        [SerializeField] private Vector2 size;
        
        [Space]
        
        [SerializeField] private NavigationArea from;
        [SerializeField] private NavigationArea to;
        [SerializeField] private bool bidirectional = true;

        public NavigationArea From => from;

        public NavigationAreaNode AreaFromNode => _areaFromNode;
        
        public NavigationArea To => to;

        public NavigationAreaNode AreaToNode => _areaToNode;

        public bool Bidirectional => bidirectional;
        
        private void Awake()
        {
            _areaFromNode = new NavigationAreaNode(transform.position - transform.forward * (size.y / 2f));
            _areaToNode = new NavigationAreaNode(transform.position + transform.forward * (size.y / 2f));

            NavigationNode fromConnection = from.WorldPositionToNode(_areaFromNode.Position);
            NavigationNode toConnection = to.WorldPositionToNode(_areaToNode.Position);
            
            if (fromConnection == null || toConnection == null)
                throw new Exception($"Navigation Area Link {name} is not positioned correctly");
            
            _areaFromNode.AddConnection(new NavigationAreaConnection(_areaFromNode, fromConnection, (_areaFromNode.Position - fromConnection.Position).magnitude));
            fromConnection.AddConnection(new NavigationAreaConnection(fromConnection, _areaFromNode, (fromConnection.Position - _areaFromNode.Position).magnitude));
            
            _areaToNode.AddConnection(new NavigationAreaConnection(_areaToNode, toConnection, (_areaToNode.Position - toConnection.Position).magnitude));
            fromConnection.AddConnection(new NavigationAreaConnection(toConnection, _areaToNode, (toConnection.Position - _areaToNode.Position).magnitude));
        }

        private void OnEnable()
        {
            NavigationWorld.Links.Add(this);
        }

        private void OnDrawGizmosSelected()
        {
            // Draw link boundaries
            Vector3 bottomLeft = transform.position -
                                 transform.forward * (size.y / 2f) -
                                 transform.right * (size.x / 2f);

            Vector3 topRight = transform.position +
                               transform.forward * (size.y / 2f) +
                               transform.right * (size.x / 2f);
            
            Vector3 bottomRight = transform.position -
                                  transform.forward * (size.y / 2f) +
                                  transform.right * (size.x / 2f);
            
            Vector3 topLeft = transform.position +
                               transform.forward * (size.y / 2f) -
                               transform.right * (size.x / 2f);

            Gizmos.color = Color.red;
            Gizmos.DrawLine(bottomLeft, bottomRight);
            Gizmos.DrawLine(bottomRight, topRight);
            Gizmos.DrawLine(topRight, topLeft);
            Gizmos.DrawLine(topLeft, bottomLeft);
            
            // Draw link nodes
            if (!Application.isPlaying)
            {
                Vector3 toNodePos = topLeft + (topRight - topLeft) / 2;
                Vector3 fromNodePos = bottomLeft + (bottomRight - bottomLeft) / 2;
                
                Gizmos.color = Color.green;
                Gizmos.DrawSphere(fromNodePos, 0.1f);

                Gizmos.color = Color.red;
                Gizmos.DrawSphere(toNodePos, 0.1f);
            }
            else
            {
                Gizmos.color = Color.green;
                Gizmos.DrawSphere(_areaFromNode.Position, 0.1f);

                Gizmos.color = Color.red;
                Gizmos.DrawSphere(_areaToNode.Position, 0.1f);
            }
        }
    }
}