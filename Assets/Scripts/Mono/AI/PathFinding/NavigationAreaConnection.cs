﻿namespace TheDayIDied.Mono.AI.PathFinding
{
    public class NavigationAreaConnection : NavigationNodeConnection
    {
        public override NavigationNode From { get; }
        
        public override NavigationNode To { get; }
        
        public override float Cost { get; }

        public NavigationAreaConnection(NavigationNode from, NavigationNode to, float cost)
        {
            From = from;
            To = to;
            Cost = cost;
        }
    }
}