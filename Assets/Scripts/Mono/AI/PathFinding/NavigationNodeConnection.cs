﻿namespace TheDayIDied.Mono.AI.PathFinding
{
    public abstract class NavigationNodeConnection
    {
        public abstract NavigationNode From { get; }
        
        public abstract NavigationNode To { get; }
        
        public abstract float Cost { get; }
    }
}