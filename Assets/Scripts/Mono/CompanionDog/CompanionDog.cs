﻿using TheDayIDied.Mono.HouseSystems;
using UnityEngine;
using UnityEngine.AI;

namespace TheDayIDied.Mono.CompanionDog
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class CompanionDog : MonoBehaviour
    {
        private NavMeshAgent _agent;

        private void Awake()
        {
            _agent = GetComponent<NavMeshAgent>();

            Room.OnPlayerEnterRoom += OnPlayerEnterRoom;
        }

        private void OnPlayerEnterRoom(Room room)
        {
            Debug.Log($"Player entered {room.name}");
            _agent.SetDestination(room.transform.position);
        }
    }
}