using System;
using UnityEngine;

namespace TheDayIDied.Mono.UI
{
    public class TabGroup : MonoBehaviour
    {
        [SerializeField] private Tab[] tabs;

        private void Awake()
        {
            for (int i = 0; i < tabs.Length; i++)
            {
                TabButton button = tabs[i].Button;
                
                button.Index = i;
                button.onClick.AddListener(() => SwitchTab(button.Index));
            }
        }

        private void OnEnable()
        {
            if(tabs.Length > 0)
                SwitchTab(0);
        }

        private void SwitchTab(int index)
        {
            if (index > tabs.Length - 1)
                return;
            
            for (int i = 0; i < tabs.Length; i++)
            {
                tabs[i].Page.SetActive(i == index);
            }
        }
    }

    [Serializable]
    public struct Tab
    {
        public TabButton Button;
        public GameObject Page;
    }
}
