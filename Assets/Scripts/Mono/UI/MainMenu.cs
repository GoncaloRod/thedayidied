using TheDayIDied.Static;
using TheDayIDied.ScriptableObjects;
using UnityEngine;
using UnityEngine.UI;

namespace TheDayIDied.Mono.UI
{
    public class MainMenu : MonoBehaviour
    {
        private PlotProbabilities _plotProbabilities;
        
        [SerializeField] private GameObject contentPanel;
        
        [Space]
        
        [SerializeField] private Button continueButton;
        [SerializeField] private Button loadGameButton;
        [SerializeField] private Button newGameButton;
        [SerializeField] private Button settingsButton;
        [SerializeField] private Button exitGameButton;

        [Space]
        
        [SerializeField] private SettingsMenu settingsMenu;

        [Space]
        
        [SerializeField] private SaveData saveData;
        [SerializeField] private StoryLineSettings storyLineSettings;
        
        private void Awake()
        {
            // TODO: Hide continue and load game buttons if there is no saves
            
            // Assign click callbacks
            continueButton.onClick.AddListener(OnContinueButtonPressed);
            loadGameButton.onClick.AddListener(OnLoadGameButtonPressed);
            newGameButton.onClick.AddListener(OnNewGameButtonPressed);
            settingsButton.onClick.AddListener(OnSettingsButtonPressed);
            exitGameButton.onClick.AddListener(OnExitGameButtonPressed);

            settingsMenu.OnSettingsMenuClose += Show;
        }

        public void Show()
        {
            contentPanel.SetActive(true);
        }

        public void Hide()
        {
            contentPanel.SetActive(false);
        }

        private void OnContinueButtonPressed()
        {
            // TODO: Load most recent save
        }

        private void OnLoadGameButtonPressed()
        {
            // TODO: Show every save game
        }

        private void OnNewGameButtonPressed()
        {
            SaveGameHandler.NewGame(saveData, storyLineSettings);
            Debug.Log(saveData.StoryLineData.Killer);
        }

        private void OnSettingsButtonPressed()
        {
            settingsMenu.Show();
            Hide();
        }

        private void OnExitGameButtonPressed()
        {
            Application.Quit();
        }
    }
}
