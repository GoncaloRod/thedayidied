﻿using UnityEngine;
using UnityEngine.UI;

namespace TheDayIDied.Mono.UI
{
    public class TabButton : Button
    {
        public int Index { get; set; }
    }
}