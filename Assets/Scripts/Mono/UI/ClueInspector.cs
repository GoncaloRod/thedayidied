using System;
using TheDayIDied.ScriptableObjects;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace TheDayIDied.Mono.UI
{
    public class ClueInspector : MonoBehaviour
    {
        public static ClueInspector Instance { get; private set; }
        
        private InputActions _actions;
        
        private ClueData _clue;

        private bool _visible = false;

        [SerializeField] private GameObject container;
        [SerializeField] private Image clueImage;
        [SerializeField] private TMP_Text clueName;

        public Action OnClueInspectorShow { get; set; }
        public Action OnClueInspectorHide { get; set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }

            _actions = new InputActions();

            _actions.UI.Escape.performed += ctx =>
            {
                if (_visible)
                    Hide();
            };
        }

        public void Show(ClueData clue)
        {
            _clue = clue;
            
            UpdateGraphics();
            
            container.SetActive(true);
            OnClueInspectorShow();
            _visible = true;
        }

        public void Hide()
        {
            container.SetActive(false);
            OnClueInspectorHide();
            _visible = false;
        }

        private void UpdateGraphics()
        {
            clueImage.sprite = _clue.Sprite;
            clueName.text = _clue.Description;
        }

        private void OnEnable()
        {
            _actions.Enable();
        }

        private void OnDisable()
        {
            _actions.Disable();
        }
    }
}
