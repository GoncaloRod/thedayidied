﻿using TheDayIDied.ScriptableObjects;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace TheDayIDied.Mono.UI
{
    [RequireComponent(typeof(RectTransform))]
    [RequireComponent(typeof(CanvasGroup))]
    public class ClueBoardClue : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler, IDropHandler
    {
        private ClueData _clueData;

        private Canvas _canvas;
        private RectTransform _rectTransform;
        private CanvasGroup _canvasGroup;
        
        [SerializeField] private Image clueImage;

        public ClueBoard ClueBoard { get; set; }

        public ClueData ClueData
        {
            get => _clueData;
            set
            {
                _clueData = value;
                UpdateClue();
            }
        }

        private void Awake()
        {
            _canvas = GetComponentInParent<Canvas>();
            
            _rectTransform = GetComponent<RectTransform>();
            _canvasGroup = GetComponent<CanvasGroup>();
        }

        private void UpdateClue()
        {
            if (_clueData == null)
                return;

            clueImage.sprite = _clueData.Sprite;
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            _canvasGroup.blocksRaycasts = false;
            _canvasGroup.alpha = 0.5f;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            _canvasGroup.blocksRaycasts = true;
            _canvasGroup.alpha = 1f;
        }

        public void OnDrag(PointerEventData eventData)
        {
            _rectTransform.anchoredPosition += eventData.delta / _canvas.scaleFactor;
        }

        public void OnDrop(PointerEventData eventData)
        {
            GameObject other = eventData.pointerDrag;
            
            if (other == null)
                return;

            ClueBoardClue clue = other.GetComponent<ClueBoardClue>();
            
            if (clue == null)
                return;
            
            //Debug.Log($"Crafting {_clueData.Description} with {clue.ClueData.Description}");
            ClueBoard.TryCraft(_clueData, clue.ClueData);
        }
    }
}