﻿using System;
using UnityEngine;

namespace TheDayIDied.Mono.UI
{
    public class SettingsMenu : MonoBehaviour
    {
        [SerializeField] private GameObject _contentPanel;

        public Action OnSettingsMenuClose;
        
        private void Awake()
        {
            if (_contentPanel.activeSelf)
                Hide();
        }

        public void Show()
        {
            _contentPanel.SetActive(true);
        }

        public void Hide()
        {
            _contentPanel.SetActive(false);
        }

        public void Close()
        {
            OnSettingsMenuClose?.Invoke();
        }
    }
}