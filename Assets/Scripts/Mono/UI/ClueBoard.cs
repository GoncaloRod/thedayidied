using TheDayIDied.Mono.Player;
using TheDayIDied.Mono.UI;
using TheDayIDied.ScriptableObjects;
using UnityEngine;

namespace TheDayIDied
{
    [RequireComponent(typeof(PlayerInventory))]
    public class ClueBoard : MonoBehaviour
    {
        private PlayerInventory _inventory;
        
        [SerializeField] private ClueRecipes recipes;
        
        [Space]
        [SerializeField] private GameObject cluePrefab;

        private void Awake()
        {
            _inventory = GetComponent<PlayerInventory>();

            foreach (ClueData clueData in _inventory.ClueInventory.Clues)
            {
                InstantiateClueObject(clueData);
            }
        }
        
        public void TryCraft(ClueData clueA, ClueData clueB)
        {
            // TODO: Is there a better way to do this?????
            foreach (Recipe recipe in recipes.Recipes)
            {
                if ((recipe.ClueA == clueA && recipe.ClueB == clueB) || (recipe.ClueA == clueB && recipe.ClueB == clueA))
                {
                    ClueData result = recipe.Result;
                    
                    _inventory.ClueInventory.Add(result);
                    InstantiateClueObject(result);
                    return;
                }
            }
        }

        private void InstantiateClueObject(ClueData clueData)
        {
            GameObject instance = Instantiate(cluePrefab, transform);

            ClueBoardClue clue = instance.GetComponent<ClueBoardClue>();

            clue.ClueBoard = this;
            clue.ClueData = clueData;
        }
    }
}
