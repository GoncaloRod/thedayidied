﻿using System.Collections.Generic;
using UnityEngine;

namespace TheDayIDied.Mono.HouseSystems
{
    public class House : MonoBehaviour
    {
        private List<Activity> _completedActivities = new List<Activity>();
        
        [SerializeField] private List<Room> rooms = new List<Room>();
        
        public List<Activity> activities = new List<Activity>();

        public List<Room> Rooms => rooms;
        public List<Activity> Activities => activities;


        private void Start()
        {
            GetActivities();
        }

        private void Update()
        {
            foreach (var activity in activities)
            {
                if (!activity.ActivityAvailability)
                {
                    activities.Remove(activity);
                    _completedActivities.Add(activity);
                }
            }
        }

        private void GetActivities()
        {
            foreach (var room in rooms)
            {
                activities.AddRange(room.Activities);
            }
        }

        public void CreateRoom()
        {
            GameObject room = new GameObject($"room {rooms.Count}");

            room.AddComponent<Room>();

            room.transform.SetParent(this.transform);
            room.transform.localPosition = new Vector3(0, 0, 0);
            
            AddToList(room);
        }

        private void AddToList(GameObject go)
        {
            Room room = go.GetComponent<Room>();
            
            rooms.Add(room);
        }
        
        
    }
}