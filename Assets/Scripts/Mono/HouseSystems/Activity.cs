﻿using UnityEngine;

namespace TheDayIDied.Mono.HouseSystems
{
    public class Activity : MonoBehaviour
    {
        private bool activityAvailable = true;
        
        private float _fatigue;
        
        [SerializeField] private bool isActivityTiring;

        public bool ActivityAvailability => activityAvailable;

        private void Start()
        {
            _fatigue = isActivityTiring ? 10f : -10f;
        }

        public void CompleteActivity()
        {
            activityAvailable = false;
        }
    }
}