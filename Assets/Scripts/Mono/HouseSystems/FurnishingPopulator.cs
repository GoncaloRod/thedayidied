﻿using System;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

namespace TheDayIDied.Mono.HouseSystems
{
    public class FurnishingPopulator : MonoBehaviour
    {
        [Space]
        [Tooltip("Size of the preview circle, doesn't affect spawning")]
        [SerializeField] private float previewItemSize = 0.1f;

        [Header("Grid Settings")]
        [SerializeField] private Vector3 rootOffset;
        [SerializeField] private Vector3Int gridSize = Vector3Int.one;
        [SerializeField] private Vector3 gridOffset = Vector3.zero;

        [Header("Spawn Settings")]
        [SerializeField] [Range(0f, 1f)] private float spawnAmmount = 1f;
        [SerializeField] private GameObject[] itemsToSpawn;
        
        private Vector3 StartPos => transform.position + transform.right * rootOffset.x +
                                    transform.up * rootOffset.y +
                                    transform.forward * rootOffset.z;

        private void Start()
        {
            if (itemsToSpawn.Length > 0)
                SpawnItems();
        }

        private void SpawnItems()
        {
            for (int z = 0; z < gridSize.z; z++)
            {
                for (int y = 0; y < gridSize.y; y++)
                {
                    for (int x = 0; x < gridSize.x; x++)
                    {
                        bool spawn = Random.Range(0f, 1f) < spawnAmmount;
                        
                        if (!spawn) continue;

                        GameObject itemToSpawn = itemsToSpawn[Random.Range(0, itemsToSpawn.Length)];
                        Instantiate(itemToSpawn, GetItemPosition(x, y, z), Quaternion.identity, transform);
                    }
                }
            }
        }

        private Vector3 GetItemPosition(int x, int y, int z)
        {
            return StartPos + transform.right * (gridOffset.x * x) +
                              transform.up * (gridOffset.y * y) +
                              transform.forward * (gridOffset.z * z);
        }
        
        private void OnDrawGizmosSelected()
        {
            for (int z = 0; z < gridSize.z; z++)
            {
                for (int y = 0; y < gridSize.y; y++)
                {
                    for (int x = 0; x < gridSize.x; x++)
                    {
                        Gizmos.DrawWireSphere(GetItemPosition(x, y, z), previewItemSize / 2f);
                    }
                }
            }
        }
    }
}