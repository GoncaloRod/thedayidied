﻿using System;
using System.Collections.Generic;
using TheDayIDied.Mono.Interactables.Electronics;
using TheDayIDied.Mono.NPC.AI;
using UnityEngine;

namespace TheDayIDied.Mono.HouseSystems
{
    [RequireComponent(typeof(BoxCollider))]
    public class Room : MonoBehaviour
    {
        public static Action<Room> OnPlayerEnterRoom;
        public static Action<Room> OnPlayerExitRoom;

        private List<Activity> _activities = new List<Activity>();
        private List<ElectronicDevice> _eletronics = new List<ElectronicDevice>();

        private BoxCollider[] _colliders;

        private bool isNpcInRoom = false;

        [Header("Layers")] [SerializeField] private LayerMask _activityLayer;
        [SerializeField] private LayerMask _electronicsLayer;
        [SerializeField] private LayerMask _npcLayer;

        public bool NpcInRoom => isNpcInRoom;

        public List<Activity> Activities => _activities;

        private void Start()
        {
            _colliders = GetComponents<BoxCollider>();

            CheckActivitiesInRoom();
            CheckElectronicsInRoom();
        }

        private void CheckActivitiesInRoom()
        {
            for (int i = 0; i < _colliders.Length; i++)
            { 
                Collider[] others = Physics.OverlapBox(
                    transform.position + _colliders[i].center,
                    _colliders[i].size / 2,
                    transform.rotation,
                    _activityLayer
                );

                foreach (var other in others)
                {
                    Activity activity = other.GetComponent<Activity>();

                    if (activity != null)
                    {
                        _activities.Add(activity);
                    }
                }
            }
        }

        private void CheckElectronicsInRoom()
        {
            for (int i = 0; i < _colliders.Length; i++)
            {
                Collider[] others = Physics.OverlapBox(
                    transform.position + _colliders[i].center,
                    _colliders[i].size / 2,
                    transform.rotation,
                    _electronicsLayer
                );

                foreach (var other in others)
                {
                    ElectronicDevice electronicDevice = other.GetComponent<ElectronicDevice>();

                    if (electronicDevice != null)
                    {
                        _eletronics.Add(electronicDevice);
                    }
                }
            }
        }

        public void IsNpcInRoom(HumanAgent humanAgent)
        {
            for (int i = 0; i < _colliders.Length; i++)
            {
                Collider[] others = Physics.OverlapBox(
                    transform.position + _colliders[i].center,
                    _colliders[i].size / 2,
                    transform.rotation,
                    _electronicsLayer
                );

                foreach (var other in others)
                {
                    HumanAgent hA = other.GetComponent<HumanAgent>();

                    if (humanAgent.Equals(hA))
                    {
                        isNpcInRoom = true;
                    }
                }
            }
            
            isNpcInRoom = false;
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.green;
            foreach (var activity in _activities)
            {
                Gizmos.DrawWireSphere(activity.transform.position, 0.5f);
            }

            Gizmos.color = Color.blue;
            foreach (var electronic in _eletronics)
            {
                Gizmos.DrawWireSphere(electronic.transform.position, 0.5f);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            bool isPlayer = other.CompareTag("Player");
            if (isPlayer)
            {
                OnPlayerEnterRoom?.Invoke(this);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            bool isPlayer = other.CompareTag("Player");
            if (isPlayer)
            {
                OnPlayerExitRoom?.Invoke(this);
            }
        }
    }
}