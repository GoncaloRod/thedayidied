using System;
using System.Collections;
using System.Collections.Generic;
using TheDayIDied.Mono.Player;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.VFX;

namespace TheDayIDied.Mono.NPC
{
    public enum Emotion {Angry, Nervous, Overwhelmed, Calm }
    
    public class Mood : MonoBehaviour
    {
        #region Variables
        
        private VisualEffect _visualEffect;
        private Dictionary<Emotion, Color> _moodColors;
        private bool _activeState = false;
        private Emotion _currentEmotion;
        
        #endregion

        #region Properties
        
        public Emotion CurrentEmotion
        {
            get => _currentEmotion;
            set
            {
                _currentEmotion = value;
                ChangeColor();
            }
        }
        
        #endregion
        
        #region Methods
        
         // Start is called before the first frame update
         private void Start()
         {
             _currentEmotion = Emotion.Calm;
                    
             _moodColors = new Dictionary<Emotion, Color>()
             {
                 {Emotion.Angry, Color.red},
                 {Emotion.Nervous, Color.yellow},
                 {Emotion.Overwhelmed, Color.gray},
                 {Emotion.Calm, Color.green}
             };
                    
             _visualEffect = GetComponentInChildren<VisualEffect>();
             _visualEffect.Stop();
             
             ChangeColor();
         }
                
        private void ChangeColor()
        {
            _visualEffect.SetVector4("MoodColor", _moodColors[_currentEmotion]);
        }

        public void ShowMood()
        {
            _visualEffect.Play();
        }

        public void StopMood()
        {
            _visualEffect.Stop();
        }

        public void ToggleMood()
        {
            _activeState = !_activeState;

            if (_activeState)
            {
                ShowMood();
            }
            else
            {
                StopMood();
            }
        }

        private void OnEnable()
        {
            PlayerMoodAbility.MoodVFXSystems.Add(this);
        }

        private void OnDisable()
        {
            PlayerMoodAbility.MoodVFXSystems.Remove(this);
        }

        #endregion
    }
}
