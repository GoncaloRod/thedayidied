﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace TheDayIDied.Mono.NPC.AI
{
    [ExecuteInEditMode]
    public class HumanSensor : MonoBehaviour
    {
        //Sensor Variables
        [SerializeField] private float range = 5;
        [SerializeField] private float angle = 45;
        [SerializeField] private float height = 1.0f;
        [SerializeField] private LayerMask layers;
        [SerializeField] private LayerMask occlusionLayers;

        //Debug
        [SerializeField] private Color meshColor = Color.cyan;
        
        private Mesh _mesh;
        private int _scanFrequency = 30;
        private List<GameObject> _objects = new List<GameObject>();
        private int count;
        private Collider[] _colliders = new Collider[50];
        private float _scanInterval;
        private float _scanTimer;


        public List<GameObject> Objects
        {
            get
            {
                _objects.RemoveAll(obj => !obj);
                return _objects;
            }
        }
        
        private void Start()
        {
            _scanInterval = 1.0f / _scanFrequency;
        }

        private void Update()
        {
            _scanTimer -= Time.deltaTime;
            if (_scanTimer < 0)
            {
                _scanTimer += _scanInterval;
                Scan();
            }
        }

        private void Scan()
        {
            count = Physics.OverlapSphereNonAlloc(transform.position, range, _colliders, layers, QueryTriggerInteraction.Collide);
            
            _objects.Clear();
            for (int i = 0; i < count; i++)
            {
                GameObject obj = _colliders[i].gameObject;

                if (IsInSight(obj))
                {
                    _objects.Add(obj);
                }
            }
        }

        public bool IsInSight(GameObject obj)
        {
            Vector3 origin = transform.position;
            Vector3 dest = obj.transform.position;
            Vector3 direction = dest - origin;

            if (direction.y < 0 || direction.y > height)
            {
                return false;
            }

            direction.y = 0;

            float deltaAngle = Vector3.Angle(direction, transform.forward);

            if (deltaAngle > angle)
            {
                return false;
            }

            origin.y += height / 2;
            dest.y = origin.y;

            if (Physics.Linecast(origin, dest, occlusionLayers))
            {
                return false;
            }

            
            return true;
        }

        Mesh CreateMesh()
        {
            Mesh mesh = new Mesh();

            int segments = 10;
            int numTriangles = segments * 4 + 2 + 2;
            int numVertices = numTriangles * 3;
            

            Vector3[] vertices = new Vector3[numVertices];
            int[] triangles = new int[numVertices];

            Vector3 bottomCenter = Vector3.zero;
            Vector3 bottomLeft = Quaternion.Euler(0, -angle, 0) * Vector3.forward * range;
            Vector3 bottomRight = Quaternion.Euler(0, angle, 0) * Vector3.forward * range;

            Vector3 topCenter = bottomCenter + Vector3.up * height;
            Vector3 topLeft = bottomLeft + Vector3.up * height;
            Vector3 topRight = bottomRight + Vector3.up * height;

            int vert = 0;
            
            //left side
            vertices[vert++] = bottomCenter;
            vertices[vert++] = bottomLeft;
            vertices[vert++] = topLeft;
            
            vertices[vert++] = topLeft;
            vertices[vert++] = topCenter;
            vertices[vert++] = bottomCenter;
            
            //right side
            vertices[vert++] = bottomCenter;
            vertices[vert++] = topCenter;
            vertices[vert++] = topRight;
            
            vertices[vert++] = topRight;
            vertices[vert++] = bottomRight;
            vertices[vert++] = bottomCenter;

            float currentAngle = -angle;
            float deltaAngle = (angle * 2) / segments;

            for (int i = 0; i < segments; i++)
            {
                bottomLeft = Quaternion.Euler(0, currentAngle, 0) * Vector3.forward * range;
                bottomRight = Quaternion.Euler(0, currentAngle + deltaAngle, 0) * Vector3.forward * range;

                topLeft = bottomLeft + Vector3.up * height;
                topRight = bottomRight + Vector3.up * height;
                
                //far side
                vertices[vert++] = bottomLeft;
                vertices[vert++] = bottomRight;
                vertices[vert++] = topRight;
            
                vertices[vert++] = topRight;
                vertices[vert++] = topLeft;
                vertices[vert++] = bottomLeft;
                // top
                vertices[vert++] = topCenter;
                vertices[vert++] = topLeft;
                vertices[vert++] = topRight;
            
                // bottom
                vertices[vert++] = bottomCenter;
                vertices[vert++] = bottomRight;
                vertices[vert++] = bottomLeft;
                
                currentAngle += deltaAngle;
            }

            for (int i = 0; i < numVertices; i++)
            {
                triangles[i] = i;
            }

            mesh.vertices = vertices;
            mesh.triangles = triangles;
            mesh.RecalculateNormals();
            
            return mesh;
        }

        public int Filter(GameObject[] buffer, string layerName)
        {
            int layer = LayerMask.NameToLayer(layerName);
            int count = 0;
            foreach (var obj in _objects)
            {
                if (obj.layer == layer)
                {
                    buffer[count++] = obj;
                }

                if (buffer.Length == count)
                {
                    break;
                } // Buffer is full
            }

            return count;
        }
        
        private void OnValidate()
        {
            _mesh = CreateMesh();
            
            _scanInterval = 1.0f / _scanFrequency;
        }

        private void OnDrawGizmosSelected()
        {
            if (_mesh)
            {
                Gizmos.color = meshColor;
                Gizmos.DrawMesh(_mesh, transform.position, transform.rotation);
            }
            
            Gizmos.DrawWireSphere(transform.position, range);

            for (int i = 0; i < count; i++)
            {
                Gizmos.DrawSphere(_colliders[i].transform.position, 0.2f);
            }

            Gizmos.color = Color.green;
            foreach (var obj in Objects)
            {
                Gizmos.DrawSphere(obj.transform.position, 1f);
            }
        }
    }
}