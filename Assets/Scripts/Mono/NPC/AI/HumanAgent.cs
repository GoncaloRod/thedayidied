using System.Collections.Generic;
using TheDayIDied.AI.BehaviourTrees;
using TheDayIDied.Mono.HouseSystems;
using TheDayIDied.Mono.Interactables.Electronics;
using TheDayIDied.Mono.Interactables.Electronics.Devices;
using UnityEngine;
using UnityEngine.AI;

namespace TheDayIDied.Mono.NPC.AI
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class HumanAgent : MonoBehaviour, IBehaviourAgent
    {
        // AI Components
        private HumanSensor _sensor;
        private Mood _mood;
        private NavMeshAgent _navMeshAgent;

        // Class Variables
        private float _sightTime; //Time to mark the sighting of something weird
        private float _timeSinceSight;
        private bool _isNPCKiller;
        private bool _isNPCInsideItsHouse;

        [SerializeField] private BehaviourTree tree;
        [SerializeField] private House house;

        public BehaviourTree BehaviourTree => tree;

        public float TimeSinceSight => _timeSinceSight;

        private void Start()
        {
            _sensor = GetComponent<HumanSensor>();
            _mood = GetComponent<Mood>();
            _navMeshAgent = GetComponent<NavMeshAgent>();

            tree = tree.Clone();
            tree.Bind(this);
            SendActivitiesToBlackboard();
        }

        private void Update()
        {
            if (IsLevitatingObjectInSight())
            {
                _sightTime = Time.time;
                tree.blackboard.doesNPCSeeLevitatingObject = true;
            }
            else
            {
                tree.blackboard.doesNPCSeeLevitatingObject = false;
            }

            _timeSinceSight = Time.time - _sightTime;

            ChangeMood();

            tree.Update();
        }

        private bool IsLevitatingObjectInSight()
        {
            GameObject[] levitatingObjects = new GameObject[1];

            return _sensor.Filter(levitatingObjects, "Levitating") > 0;
        }

        private void ChangeMood()
        {
            if (tree.blackboard.moodPoints < 20)
            {
                _mood.CurrentEmotion = Emotion.Calm;
            }
            else if (tree.blackboard.moodPoints > 21 && tree.blackboard.moodPoints < 40)
            {
                _mood.CurrentEmotion = Emotion.Nervous;
            }
            else if (tree.blackboard.moodPoints > 41 && tree.blackboard.moodPoints < 60)
            {
                _mood.CurrentEmotion = Emotion.Overwhelmed;
            }
        }

        private void SendActivitiesToBlackboard()
        {
            tree.blackboard.availableActivities = house.Activities;
        }

        /// <summary>
        /// Method to be called when player is messing with the electronic devices
        /// </summary>
        public void ElectronicDeviceReaction(ElectronicDevice electronicDevice)
        {
            _sightTime = Time.time;

            if (electronicDevice is LightSwitch)
            {
                tree.blackboard.moodPoints += 5;

                Debug.Log("Reaction to the Lights");
                //TODO: Goto Light Switch // Prob to be implemented on the BT
                //TODO: Play Animation
            }
            else if (electronicDevice is Television || electronicDevice is Radio)
            {
                Debug.Log("Reaction to the Television");
                tree.blackboard.moodPoints += 3;
                //TODO: Goto Television or Radio // Prob to be implemented on the BT
                //TODO: Play Animation
            }
        }

        /// <summary>
        /// This method sets a path to a activity
        /// </summary>
        public bool SetDestinationActivity(Transform destinationActivity)
        {
            var targetPosition = destinationActivity.position;
            _navMeshAgent.SetDestination(targetPosition);

            float dist = Vector2.Distance(new Vector2(transform.position.x, transform.position.z),
                new Vector2(targetPosition.x, targetPosition.z));

            return dist <= _navMeshAgent.stoppingDistance;
        }
    }
}