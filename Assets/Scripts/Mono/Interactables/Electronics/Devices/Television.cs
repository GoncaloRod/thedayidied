using UnityEngine;

namespace TheDayIDied.Mono.Interactables.Electronics.Devices
{
    public class Television : ElectronicDevice
    {
        protected override void ApplyChanges()
        {
            Debug.Log($"{name} is now {(PowerState ? "on" : "off")}");
            
            // TODO: Actually turn off the TV
        }
    }
}
