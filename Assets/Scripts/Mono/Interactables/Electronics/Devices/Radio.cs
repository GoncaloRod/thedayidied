using UnityEngine;

namespace TheDayIDied.Mono.Interactables.Electronics.Devices
{
    public class Radio : ElectronicDevice
    {
        protected override void ApplyChanges()
        {
            Debug.Log($"{name} is now {(PowerState ? "on" : "off")}");
            
            // TODO: Actually stop playing music
        }
    }
}
