using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace TheDayIDied.Mono.Interactables.Electronics.Devices
{
    public class LightSwitch : ElectronicDevice
    {
        private bool _flicking = false;

        [Space]
        [SerializeField] private Light[] lights;

        protected override void ApplyChanges()
        {
            if (lights.Length == 0)
                return;
            
            foreach (Light light in lights)
                if (light != null)
                    light.enabled = PowerState;
        }

        protected override void RegisterDeviceSpecificInteractions()
        {
            Interactions.Add(new Interaction(){ Name = "Flicker Lights", Action = FlickerLights });
        }

        private void FlickerLights()
        {
            if (!_flicking)
            {
                StartCoroutine(FlickingRoutine());
                CheckHumansInRange();
            }
            else
            {
                _flicking = false;
            }
        }

        private IEnumerator FlickingRoutine()
        {
            _flicking = true;
            
            while (_flicking)
            {
                PowerState = Random.Range(0, 2) == 1;

                foreach (Light light in lights)
                    light.enabled = PowerState;

                yield return new WaitForSeconds(0.25f);
            }
        }

        protected override void DrawDeviceGizmos()
        {
            if (lights.Length == 0)
                return;

            Gizmos.color = Color.yellow;
            
            foreach (Light light in lights)
                if (light != null)
                    Gizmos.DrawLine(transform.position, light.transform.position);
        }
    }
}
