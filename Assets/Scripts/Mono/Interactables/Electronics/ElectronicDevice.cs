using TheDayIDied.Mono.NPC.AI;
using UnityEngine;
using UnityEngine.Serialization;

namespace TheDayIDied.Mono.Interactables.Electronics
{
    public abstract class ElectronicDevice : Interactable
    {
        [Space]
        [SerializeField] private bool initialPowerState = true;

        [Space]
        
        [SerializeField] private Vector3 rangeBoxCenter;
        [SerializeField] private Vector3 rangeBoxSize = Vector3.one;
        [SerializeField] [FormerlySerializedAs("NPC Layer")] private LayerMask npcLayer;
        
        public bool PowerState { get; protected set; }

        /// <summary>
        /// Applies specific visual changes to electronic device.
        /// Ex.: Turn light on or off, stop or start music from radio, ...
        /// </summary>
        protected abstract void ApplyChanges();
        
        /// <summary>
        /// Register device specific interactions.
        /// Ex.: A light can be flickered and a TV doesn't and a TV can change channels and a light doesn't.
        /// </summary>
        protected virtual void RegisterDeviceSpecificInteractions() {  }
        
        protected virtual void DrawDeviceGizmos() { }

        protected override void InitInteractable()
        {
            PowerState = initialPowerState;
            
            ApplyChanges();

            Interactions.Add(new Interaction(){ Name = "Toggle Power", Action = TogglePower });
                
            RegisterDeviceSpecificInteractions();
            
            InitDevice();
        }
        
        protected virtual void InitDevice() { }
        
        public void TogglePower()
        {
            PowerState = !PowerState;
            
            ApplyChanges();
            
            CheckHumansInRange();
        }

        protected void CheckHumansInRange()
        {
            Collider[] others = Physics.OverlapBox(
                transform.position + rangeBoxCenter,
                rangeBoxSize / 2f,
                Quaternion.identity,
                npcLayer
            );

            foreach (var other in others)
            {
                HumanAgent humanAgent = other.GetComponent<HumanAgent>();

                if (humanAgent != null)
                {
                    humanAgent.ElectronicDeviceReaction(this);
                }
            }
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireCube(transform.position + rangeBoxCenter, rangeBoxSize);
            
            DrawDeviceGizmos();
        }
    }
}
