﻿using System;
using TheDayIDied.Mono.UI;
using TheDayIDied.ScriptableObjects;
using UnityEngine;

namespace TheDayIDied.Mono.Interactables.Clues
{
    public class Clue : Interactable
    {
        private ClueInspector _clueInspector;
        
        [SerializeField] private ClueData clueData;

        public ClueData ClueData => clueData;
        
        protected override void InitInteractable()
        {
            _clueInspector = ClueInspector.Instance;
            
            Interactions.Add(new Interaction(){ Action = Inspect, Name = "Inspect" });
        }

        private void Inspect()
        {
            _clueInspector.Show(clueData);
        }
    }
}