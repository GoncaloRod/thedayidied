﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace TheDayIDied.Mono.Interactables
{
    public abstract class Interactable : MonoBehaviour
    {
        private int _currentInteractionIndexIndex = 0;

        [SerializeField] private InteractionDisplay interactionDisplay;
        
        protected List<Interaction> Interactions { get; } = new List<Interaction>();
        
        public int CurrentInteractionIndex
        {
            get => _currentInteractionIndexIndex;
            
            set
            {
                if (value < 0)
                    _currentInteractionIndexIndex = Interactions.Count - 1;
                else if (value > Interactions.Count - 1)
                    _currentInteractionIndexIndex = 0;
                else
                    _currentInteractionIndexIndex = value;
                
                UpdateInteractionDisplay();
            }
        }

        public Interaction CurrentInteraction => Interactions[_currentInteractionIndexIndex];

        private void Start()
        {
            InitInteractable();
            
            if (interactionDisplay != null)
            {
                interactionDisplay.ShowArrows = Interactions.Count > 1;
            }
        }
        
        protected virtual void InitInteractable() { }

        public void Interact()
        {
            CurrentInteraction.Action();
        }

        public void ShowInteractionDisplay()
        {
            if (interactionDisplay == null) return;
            
            UpdateInteractionDisplay();
            interactionDisplay.Show();
        }

        public void HideInteractionDisplay()
        {
            if (interactionDisplay == null) return;
            
            UpdateInteractionDisplay();
            interactionDisplay.Hide();
        }

        public void UpdateInteractionDisplay()
        {
            if (interactionDisplay == null) return;
            
            interactionDisplay.InteractionName = CurrentInteraction.Name;
        }
    }

    public struct Interaction
    {
        public string Name;
        public Action Action;
    }
}