using TMPro;
using UnityEngine;

namespace TheDayIDied
{
    public class InteractionDisplay : MonoBehaviour
    {
        private bool _showArrows;
        
        [SerializeField] private TMP_Text interactionName;
        [SerializeField] private GameObject[] arrows;

        public string InteractionName
        {
            get => interactionName.text;
            set => interactionName.text = value;
        }

        public bool ShowArrows
        {
            get => _showArrows;
            set
            {
                _showArrows = value;
                UpdateArrows();
            }
        }

        public void Show()
        {
            // TODO: Use tweening function
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            // TODO: Use tweening function
            gameObject.SetActive(false);
        }

        private void UpdateArrows()
        {
            foreach (GameObject arrow in arrows)
            {
                arrow.SetActive(_showArrows);
            }
        }
    }
}
