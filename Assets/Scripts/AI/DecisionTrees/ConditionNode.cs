﻿using System;
using TMPro;
using UnityEngine;

namespace TheDayIDied.AI.DecisionTrees
{
    public abstract class ConditionNode : DecisionNode
    {
        private DecisionNode _trueChild, _falsechild;

        public ConditionNode(string name, DecisionNode trueChild, DecisionNode falsechild)
        {
            Name = name;
            _trueChild = trueChild;
            _falsechild = falsechild;
        }

        /*
        public override void Run()
        {
            if (_condition()) _trueChild.Run();
            else _falsechild.Run();
        }*/
    }
}