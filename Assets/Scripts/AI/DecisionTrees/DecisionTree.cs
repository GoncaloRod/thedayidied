﻿using System.Collections.Generic;
using UnityEngine;

namespace TheDayIDied.AI.DecisionTrees
{
    [CreateAssetMenu(fileName = "DecisionTree", menuName = "AI/DecisionTree", order = 0)]
    public class DecisionTree : ScriptableObject
    {
        [SerializeField] private ConditionNode _rootNode;
        [SerializeField] private List<DecisionNode> nodes;

    }
}