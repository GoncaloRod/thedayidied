using UnityEngine;

namespace TheDayIDied.AI.DecisionTrees
{
    [System.Serializable]
    public abstract class DecisionNode : ScriptableObject
    {
        public string Name { protected set; get; }

        public abstract void Run();

    }
}
