﻿using UnityEngine;

namespace TheDayIDied.AI.DecisionTrees
{
    public abstract class DecisionActionNode : DecisionNode
    {
        protected DecisionActionNode(string name)
        {
            Name = name;
        }
    }
}