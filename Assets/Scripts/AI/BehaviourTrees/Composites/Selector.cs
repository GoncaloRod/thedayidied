namespace TheDayIDied.AI.BehaviourTrees.Composites
{
    public class Selector : CompositeNode
    {
        protected override void OnStart()
        {
            
        }

        protected override void OnStop()
        {
            
        }

        protected override NodeState OnUpdate()
        {
            foreach (var child in children)
            {
                switch (child.Update())
                {
                    case NodeState.Running:
                        nodeState = NodeState.Running;
                        return nodeState;
                    case NodeState.Success:
                        nodeState = NodeState.Success;
                        return nodeState;
                    case NodeState.Failure:
                        break;
                }
            }
            
            nodeState = NodeState.Failure;
            return nodeState;
        }
        
    }
}