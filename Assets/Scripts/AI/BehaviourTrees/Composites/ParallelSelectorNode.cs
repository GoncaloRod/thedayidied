﻿namespace TheDayIDied.AI.BehaviourTrees.Composites
{
    public class ParallelSelectorNode : CompositeNode
    {
        protected override void OnStart()
        {
        }

        protected override void OnStop()
        {
        }

        protected override NodeState OnUpdate()
        {
            foreach (var child in children)
            {
                child.Update();
            }

            return children.TrueForAll(c => c.NodeState == NodeState.Success || c.NodeState == NodeState.Failure)
                ? NodeState.Success
                : NodeState.Running;
        }
    }
}