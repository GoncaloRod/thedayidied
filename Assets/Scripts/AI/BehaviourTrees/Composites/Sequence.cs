using System;

namespace TheDayIDied.AI.BehaviourTrees.Composites
{
    public class Sequence : CompositeNode
    {
        private int current;
        
        protected override void OnStart()
        {
            current = 0;
        }

        protected override void OnStop()
        {
            
        }

        protected override NodeState OnUpdate()
        {
            var child = children[current];
            switch (child.Update())
            {
                case NodeState.Running:
                    return NodeState.Running;
                case NodeState.Success:
                    current++;
                    break;
                case NodeState.Failure:
                    return NodeState.Failure;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return current == children.Count ? NodeState.Success : NodeState.Running;
        }
    }
}
