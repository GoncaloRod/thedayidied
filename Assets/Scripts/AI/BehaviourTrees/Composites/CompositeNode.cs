﻿using System.Collections.Generic;

namespace TheDayIDied.AI.BehaviourTrees.Composites
{
    public abstract class CompositeNode : BehaviourNode
    {
        public List<BehaviourNode> children = new List<BehaviourNode>();

        public override BehaviourNode Clone()
        {
            CompositeNode btNode = Instantiate(this);
            btNode.children = children.ConvertAll(c => c.Clone());

            return btNode;
        }
    }
}