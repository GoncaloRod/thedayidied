using System.Collections;
using System.Collections.Generic;
using TheDayIDied.Mono.NPC.AI;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TheDayIDied.AI.BehaviourTrees
{
    [System.Serializable]
    public abstract class BehaviourNode : ScriptableObject
    {
        protected NodeState nodeState = NodeState.Running;
        
        [HideInInspector] public bool started = false;
        [HideInInspector] public HumanBlackboard blackboard;
        [HideInInspector] public IBehaviourAgent agent;
        
        //Editor Variables
        [HideInInspector] public string guid;
        [HideInInspector] public Vector2 pos;

        public NodeState NodeState => nodeState;

        public NodeState Update()
        {
            if (!started)
            {
                OnStart();
                started = true;
            }

            nodeState = OnUpdate();

            if (nodeState == NodeState.Failure || nodeState == NodeState.Success)
            {
                OnStop();
                started = false;
            }

            return nodeState;
        }

        public virtual BehaviourNode Clone()
        {
            return Instantiate(this);
        }

        protected abstract void OnStart();
        protected abstract void OnStop();
        protected abstract NodeState OnUpdate();
        

    }

    public enum NodeState
    {
        Running, Success, Failure
    }
}
