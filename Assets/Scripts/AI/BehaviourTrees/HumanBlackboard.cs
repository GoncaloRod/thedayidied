using System.Collections.Generic;
using TheDayIDied.Mono.HouseSystems;
using UnityEngine;
using UnityEngine.Serialization;

namespace TheDayIDied.AI.BehaviourTrees
{
    [System.Serializable]
    public class HumanBlackboard
    {
       //Variables to be known by the tree
       public float range;
       public float moodPoints;
       public float tiredness;
       

       public bool doesNPCSeeLevitatingObject;

       public List<Activity> availableActivities = new List<Activity>();
       public Transform destinationActivity;
    }
}
