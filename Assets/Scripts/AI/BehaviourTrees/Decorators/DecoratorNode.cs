﻿namespace TheDayIDied.AI.BehaviourTrees.Decorators
{
    public abstract class DecoratorNode : BehaviourNode
    {
        public BehaviourNode child;
        
        public override BehaviourNode Clone()
        {
            DecoratorNode btNode = Instantiate(this);
            btNode.child = child.Clone();

            return btNode;
        }
        
    }
}