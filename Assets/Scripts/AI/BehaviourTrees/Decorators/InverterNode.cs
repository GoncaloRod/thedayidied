﻿namespace TheDayIDied.AI.BehaviourTrees.Decorators
{
    public class InverterNode : DecoratorNode
    {
        protected override void OnStart()
        {
            
        }

        protected override void OnStop()
        {
            
        }

        protected override NodeState OnUpdate()
        {
            switch (child.Update())
            {
                case NodeState.Failure:
                    return NodeState.Success;
                case NodeState.Success:
                    return NodeState.Failure;
                case NodeState.Running:
                    break;
            }

            return NodeState.Running;
        }
    }
}