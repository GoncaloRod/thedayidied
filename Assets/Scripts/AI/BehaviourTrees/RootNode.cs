namespace TheDayIDied.AI.BehaviourTrees
{
    public class RootNode : BehaviourNode
    {
        public BehaviourNode child;

        protected override void OnStart()
        {
           
        }

        protected override void OnStop()
        {
            
        }

        protected override NodeState OnUpdate()
        {
            return child.Update();
        }
        
        public override BehaviourNode Clone()
        {
            RootNode btNode = Instantiate(this);
            btNode.child = child.Clone();

            return btNode;
        }
    }
}
