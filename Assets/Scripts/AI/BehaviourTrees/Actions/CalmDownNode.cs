using TheDayIDied.Mono.NPC.AI;
using UnityEngine;

namespace TheDayIDied.AI.BehaviourTrees.Actions
{
    public class CalmDownNode : ActionNode
    {
        private HumanAgent _humanAgent;
        
        [SerializeField] private float timeToCalmDown = 60.0f;
        
        
        
        protected override void OnStart()
        {
            if (agent is HumanAgent)
            {
                _humanAgent = agent as HumanAgent;
            }
        }

        protected override void OnStop()
        {
            
        }

        protected override NodeState OnUpdate()
        {
            if (_humanAgent.TimeSinceSight > timeToCalmDown && blackboard.moodPoints > 0)
            {
                blackboard.moodPoints -= 5;
                Debug.Log("I'm calming down!");
                return NodeState.Success;
            }
            
            return NodeState.Failure;
        }
    }
}
