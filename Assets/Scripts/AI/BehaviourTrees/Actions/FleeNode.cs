﻿using UnityEngine;

namespace TheDayIDied.AI.BehaviourTrees.Actions
{
    public class FleeNode : ActionNode
    {
        protected override void OnStart()
        {
            
        }

        protected override void OnStop()
        {
            
        }

        protected override NodeState OnUpdate()
        {
            if (blackboard.moodPoints > 60)
            {
                //TODO: Implement flee behaviour using Pathfinding
                //*remember to put it in running state while the npc is going to the target location
                
                Debug.Log("Im fleeing my house! Someone help me!");
                return NodeState.Success;

            }

            return NodeState.Failure;
        }
    }
}