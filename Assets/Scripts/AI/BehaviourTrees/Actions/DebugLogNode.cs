﻿using UnityEngine;

namespace TheDayIDied.AI.BehaviourTrees.Actions
{
    public class DebugLogNode : ActionNode
    {
        public string message;
        
        protected override void OnStart()
        {
            
        }

        protected override void OnStop()
        {
            
        }

        protected override NodeState OnUpdate()
        {
            Debug.Log($"Message {message}");
            return NodeState.Success;
        }
    }
}