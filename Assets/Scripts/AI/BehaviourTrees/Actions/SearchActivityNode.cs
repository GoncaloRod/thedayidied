﻿using System.Collections.Generic;
using TheDayIDied.Mono.NPC.AI;
using UnityEngine;

namespace TheDayIDied.AI.BehaviourTrees.Actions
{
    public class SearchActivityNode : ActionNode
    {
        protected override void OnStart()
        {
            blackboard.destinationActivity = null;
        }

        protected override void OnStop()
        {
            
        }

        protected override NodeState OnUpdate()
        {
            int random = Random.Range(0, blackboard.availableActivities.FindAll(a => a.ActivityAvailability).Count);

            if (!blackboard.availableActivities[random].ActivityAvailability)
                random++;

            blackboard.destinationActivity = blackboard.availableActivities[random].transform;

            if (blackboard.destinationActivity == null)
            {
                return NodeState.Failure;
            }

            return NodeState.Success;
        }
    }
}