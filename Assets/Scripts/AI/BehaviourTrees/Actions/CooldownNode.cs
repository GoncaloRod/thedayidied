﻿using UnityEngine;

namespace TheDayIDied.AI.BehaviourTrees.Actions
{
    public class CooldownNode : ActionNode
    {
        private float _startTime;
        
        [SerializeField] private float _duration;
        
        protected override void OnStart()
        {
            _startTime = Time.time;
        }

        protected override void OnStop()
        {
            
        }

        protected override NodeState OnUpdate()
        {
            return Time.time - _startTime > _duration ? NodeState.Success : NodeState.Running;
        }
    }
}