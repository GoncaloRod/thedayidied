﻿using TheDayIDied.Mono.NPC.AI;
using UnityEngine;

namespace TheDayIDied.AI.BehaviourTrees.Actions
{
    public class GoToActivityNode : ActionNode
    {
        private HumanAgent _humanAgent;
       
        protected override void OnStart()
        {
            if (agent is HumanAgent humanAgent)
            {
                _humanAgent = humanAgent;
            }
        }

        protected override void OnStop()
        {
            
        }

        protected override NodeState OnUpdate()
        {
            while (!_humanAgent.SetDestinationActivity(blackboard.destinationActivity))
            {
                return NodeState.Running;
            }

            return NodeState.Success;
        }
    }
}