﻿using UnityEngine;

namespace TheDayIDied.AI.BehaviourTrees.Actions
{
    public class IsLevitatingObjectInSightNode : ActionNode
    {
        protected override void OnStart()
        {
            
        }

        protected override void OnStop()
        {
            if (blackboard.doesNPCSeeLevitatingObject)
            {
                Debug.Log("Object Detected!!!");
            }
        }

        protected override NodeState OnUpdate()
        {
            return blackboard.doesNPCSeeLevitatingObject ? NodeState.Success : NodeState.Failure;
        }
    }
}