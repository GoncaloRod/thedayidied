﻿using UnityEngine;

namespace TheDayIDied.AI.BehaviourTrees.Actions
{
    public class ChangeMoodNode : ActionNode
    {
        private float _moodIncrement = 5;
        
        protected override void OnStart()
        {
            
        }

        protected override void OnStop()
        {
            
        }

        protected override NodeState OnUpdate()
        {
            Debug.Log($"Adding {_moodIncrement} points to the mood pointer");

            blackboard.moodPoints += 5;

            return NodeState.Success;
        }
    }
}