﻿using System;
using System.Collections.Generic;
using TheDayIDied.AI.BehaviourTrees.Composites;
using TheDayIDied.AI.BehaviourTrees.Decorators;
using TheDayIDied.Mono.NPC.AI;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace TheDayIDied.AI.BehaviourTrees
{
    [CreateAssetMenu(fileName = "BehaviourTree", menuName = "AI/BehaviourTree", order = 0)]
    public class BehaviourTree : ScriptableObject
    {
        public BehaviourNode rootNode;
        public NodeState treeState = NodeState.Running;
        public List<BehaviourNode> nodes = new List<BehaviourNode>();
        public HumanBlackboard blackboard = new HumanBlackboard();

        public NodeState Update()
        {
            if (rootNode.NodeState == NodeState.Running)
            {
                treeState = rootNode.Update();
            }

            return treeState;
        }

        #if UNITY_EDITOR
        public BehaviourNode CreateNode(System.Type type)
        {
            BehaviourNode behaviourNode = CreateInstance(type) as BehaviourNode;
            behaviourNode.name = type.Name;

            behaviourNode.guid = GUID.Generate().ToString();
            nodes.Add(behaviourNode);

            if (!Application.isPlaying)
                AssetDatabase.AddObjectToAsset(behaviourNode,this);
            
            AssetDatabase.SaveAssets();
            return behaviourNode;
        }

        public void DeleteNode(BehaviourNode behaviourNode)
        {
            nodes.Remove(behaviourNode);
            AssetDatabase.RemoveObjectFromAsset(behaviourNode);
            AssetDatabase.SaveAssets();
        }

        public void AddChild(BehaviourNode parent, BehaviourNode child)
        {
            RootNode root = parent as RootNode;
            if (root)
            {
                Undo.RecordObject(root, "Behaviour Tree (Add Child)");
                root.child = child;
                EditorUtility.SetDirty(root);
            }
            
            DecoratorNode decorator = parent as DecoratorNode;
            if (decorator)
            {
                Undo.RecordObject(decorator, "Behaviour Tree (Add Child)");
                decorator.child = child;
                EditorUtility.SetDirty(decorator);
            }
            
            CompositeNode composite = parent as CompositeNode;
            if (composite)
            {
                Undo.RecordObject(composite, "Behaviour Tree (Add Child)");
                composite.children.Add(child);
                EditorUtility.SetDirty(composite);
            }
        }

        public void RemoveChild(BehaviourNode parent, BehaviourNode child)
        {
            RootNode root = parent as RootNode;
            if (root)
            {
                Undo.RecordObject(root, "Behaviour Tree (Remove Child)");
                root.child = null;
                EditorUtility.SetDirty(root);
            }
            
            DecoratorNode decorator = parent as DecoratorNode;
            if (decorator)
            {
                Undo.RecordObject(decorator, "Behaviour Tree (Remove Child)");
                decorator.child = null;
                EditorUtility.SetDirty(decorator);
            }
            
            CompositeNode composite = parent as CompositeNode;
            if (composite)
            {
                Undo.RecordObject(composite, "Behaviour Tree (Remove Child)");
                composite.children.Remove(child);
                EditorUtility.SetDirty(composite);
            }
        }

        #endif
        public List<BehaviourNode> GetChildren(BehaviourNode parent)
        {
            List<BehaviourNode> children = new List<BehaviourNode>();
            
            RootNode root = parent as RootNode;
            if (root && root.child != null)
            {
                children.Add(root.child);
            }
            
            DecoratorNode decorator = parent as DecoratorNode;
            if (decorator && decorator.child != null)
            {
                children.Add(decorator.child);
            }
            
            CompositeNode composite = parent as CompositeNode;
            if (composite)
            {
                return composite.children;
            }

            return children;
        }
        

        public void Traverse(BehaviourNode node, Action<BehaviourNode> visiter)
        {
            if (node)
            {
                visiter.Invoke(node);
                var children = GetChildren(node);
                children.ForEach((n) => Traverse(n, visiter));
            }
        }
        
        public BehaviourTree Clone()
        {
            BehaviourTree tree = Instantiate(this);
            tree.rootNode = tree.rootNode.Clone();
            tree.nodes = new List<BehaviourNode>();
            Traverse(tree.rootNode, (n) =>
            {
                tree.nodes.Add(n);
            });
            return tree;
        }

        public void Bind(IBehaviourAgent agent)
        {
            Traverse(rootNode, node =>
            {
                node.agent = agent;
                node.blackboard = blackboard;
            });
        }
        
    }
}