﻿using System;
using UnityEngine;

namespace TheDayIDied.ScriptableObjects
{
    [CreateAssetMenu(fileName = "ClueRecipes", menuName = "Clue Recipes", order = -33)]
    public class ClueRecipes : ScriptableObject
    {
        public Recipe[] Recipes;
    }

    [Serializable]
    public struct Recipe
    {
        public ClueData ClueA;
        public ClueData ClueB;
        
        [Space]
        public ClueData Result;
    }
}