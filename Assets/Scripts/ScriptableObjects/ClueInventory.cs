using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace TheDayIDied.ScriptableObjects
{
    [CreateAssetMenu(fileName = "Clue Inventory", menuName = "Clue Inventory", order = -22)]
    public class ClueInventory : ScriptableObject
    {
        [SerializeField] private List<ClueData> _clues = new List<ClueData>();

        public ReadOnlyCollection<ClueData> Clues => _clues.AsReadOnly();
        
        public void Add(ClueData clueData)
        {
            if (clueData == null)
                return;

            if (clueData == _clues.Find(c => c == clueData))
                return;
            
            _clues.Add(clueData);
        }

        public void Remove(ClueData clueData)
        {
            if (clueData == null)
                return;

            if (_clues.Exists(c => c == clueData))
                _clues.Remove(clueData);
        }
    }
}
