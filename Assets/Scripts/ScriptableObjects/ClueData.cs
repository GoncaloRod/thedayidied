﻿using UnityEngine;

namespace TheDayIDied.ScriptableObjects
{
    [CreateAssetMenu(fileName = "ClueData", menuName = "Clue Data", order = -33)]
    public class ClueData : ScriptableObject
    {
        [SerializeField] private Sprite sprite;
        [SerializeField] private string description;

        public Sprite Sprite => sprite;
        
        public string Description => description;
    }
}