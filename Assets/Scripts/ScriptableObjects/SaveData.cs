﻿using UnityEngine;

namespace TheDayIDied.ScriptableObjects
{
    public enum NightStatus
    {
        InProgress,
        Finished
    }
    
    [CreateAssetMenu(fileName = "SaveData", menuName = "Save Data", order = -21)]
    public class SaveData : ScriptableObject
    {
        public int CurrentNight;
        public NightStatus CurrentNightStatus;

        public ClueInventory ClueInventory;
        public StoryLineData StoryLineData;
    }
}