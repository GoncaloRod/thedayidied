﻿using System;
using UnityEngine;

namespace TheDayIDied.ScriptableObjects
{
    [CreateAssetMenu(fileName = "StoryLineSettings", menuName = "Story Line Settings", order = -22)]
    public class StoryLineSettings : ScriptableObject
    {
        public float UnplayedStoryLineProbability;
        public float PlayedStoryLineProbability;
        
        [Space]

        public StoryLines CompletedStoryLines;
        public StoryLines AvailableStoryLines;
    }
    
    [Serializable]
    public struct StoryLines
    {
        public bool Ben;
        public bool Alex;
        public bool Jake;
        public bool Ryan;
        public bool Julie;
        public bool Alyssa;
    }
}