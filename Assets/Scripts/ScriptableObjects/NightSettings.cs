﻿using UnityEngine;

namespace TheDayIDied.ScriptableObjects
{
    [CreateAssetMenu(fileName = "NightSettings", menuName = "Night Settings", order = -44)]
    public class NightSettings : ScriptableObject
    {
        [Space]
        [Tooltip("Time in minutes that the night will last")]
        public float Duration = 10f;
        
        // TODO: Maybe set the available powers here
    }
}