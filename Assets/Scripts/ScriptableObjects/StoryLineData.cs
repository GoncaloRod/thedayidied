﻿using TheDayIDied.Static;
using UnityEngine;

namespace TheDayIDied.ScriptableObjects
{
    public enum Character
    {
        Alex,
        Jake,
        Alyssa,
        Ben,
        Ryan,
        Julie
    }
    
    [CreateAssetMenu(fileName = "StoryLineData", menuName = "Story Line Data", order = -22)]
    public class StoryLineData : ScriptableObject
    {
        public bool RyanMoneyStolen {get; private set; }
        public bool AlexHasAffairAlyssa {get; private set; }
        public bool RobinFindsAlyssaKilledBoyfriend {get; private set; }
        public bool RyanToldRobinGamblingDebts {get; private set; }
        public bool RobinFindsJakeStoleMoney {get; private set; }
        public bool RyanToldRobinAlexAffair {get; private set; }
        public bool RobinFindsAlyssaStoleMoney {get; private set; }
        public bool AlexFindsBenIsRobinFather {get; private set; }
        public bool AlexTellEveryoneBenAffair {get; private set; }
        public bool JulieParanoiaWorsens {get; private set; }
        public bool JulieCatGoesMissing {get; private set; }
        public bool RyanTellsRobinStoleMoney {get; private set; }

        public Character Killer { get; private set; }
        
        public void Generate(PlotProbabilities probabilities)
        {
            // WARNING: This is the ultimate jank, but it will do for now (hopefully I refactor if in the future)
            
            RyanMoneyStolen = true;
            
            // Alex is having an affair with Alyssa?
            if (AlexHasAffairAlyssa = RandomHelper.RandomBool(probabilities.Alex + probabilities.Jake + probabilities.Alyssa, probabilities.Ben + probabilities.Ryan + probabilities.Julie))
            {
                // Robin finds out Alyssa killed her boyfriend?
                if (RobinFindsAlyssaKilledBoyfriend = RandomHelper.RandomBool(probabilities.Alex))
                {
                    Killer = Character.Alex;
                }
                else
                {
                    // Ryan told Robin about gambling debts?
                    if (RyanToldRobinGamblingDebts = RandomHelper.RandomBool(probabilities.Jake))
                    {
                        RobinFindsJakeStoleMoney = true;
                        Killer = Character.Jake;
                    }
                    else
                    {
                        // Ryan told Robin about Alex's affair?
                        if (RyanToldRobinAlexAffair = RandomHelper.RandomBool(probabilities.Alyssa, probabilities.Julie + probabilities.Ryan))
                        {
                            RobinFindsAlyssaStoleMoney = true;
                            Killer = Character.Alyssa;
                        }
                        else
                        {
                            RyanTellsRobinStoleMoney = true;
                            JulieParanoiaWorsens = true;

                            // Julie's cat goes missing?
                            if (JulieCatGoesMissing = RandomHelper.RandomBool(probabilities.Julie, probabilities.Ryan))
                            {
                                Killer = Character.Julie;
                            }
                            else
                            {
                                Killer = Character.Ryan;
                            }
                        }
                    }
                }
            }
            else
            {
                AlexFindsBenIsRobinFather = true;
                
                // Alex tells everyone about Ben's affair?
                if (!(AlexTellEveryoneBenAffair = RandomHelper.RandomBool(probabilities.Julie + probabilities.Ryan, probabilities.Ben)))
                {
                    Killer = Character.Ben;
                }
                else
                {
                    JulieParanoiaWorsens = true;

                    // Julie's cat goes missing?
                    if (JulieCatGoesMissing = RandomHelper.RandomBool(probabilities.Julie, probabilities.Ryan))
                    {
                        Killer = Character.Julie;
                    }
                    else
                    {
                        Killer = Character.Ryan;
                    }
                }
            }
        }
    }

    public struct PlotProbabilities
    {
        public float Ben;
        public float Alex;
        public float Jake;
        public float Ryan;
        public float Julie;
        public float Alyssa;

        public void Normalize()
        {
            float sum = Ben + Alex + Jake + Ryan + Julie + Alyssa;

            float factor = 1f / sum;

            Ben *= factor;
            Alex *= factor;
            Jake *= factor;
            Ryan *= factor;
            Julie *= factor;
            Alyssa *= factor;
        }
    }
}