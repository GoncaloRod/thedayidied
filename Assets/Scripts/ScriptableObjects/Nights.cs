﻿using UnityEngine;

namespace TheDayIDied.ScriptableObjects
{
    [CreateAssetMenu(fileName = "Nights", menuName = "Nights", order = -44)]
    public class Nights : ScriptableObject
    {
        public NightSettings[] nights;
    }
}