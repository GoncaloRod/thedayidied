﻿using UnityEngine;

namespace TheDayIDied.Static
{
    public static class RandomHelper
    {
        public static bool RandomBool() => Random.Range(0, 2) == 1;

        public static bool RandomBool(float trueProbability) => Random.Range(0f, 1f) < trueProbability;

        public static bool RandomBool(float trueProbability, float falseProbability)
        {
            // Normalize probabilities
            float sum = trueProbability + falseProbability;
            float factor = 1f / sum;

            trueProbability *= factor;

            return Random.Range(0f, 1f) < trueProbability;
        }
    }
}