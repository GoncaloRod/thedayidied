﻿using TheDayIDied.ScriptableObjects;
using UnityEngine;

namespace TheDayIDied.Static
{
    public static class SaveGameHandler
    {
        public static void NewGame(SaveData save, StoryLineSettings storyLineSettings)
        {
            save.CurrentNight = 0;
            save.CurrentNightStatus = NightStatus.InProgress;

            float unplayedProbability = storyLineSettings.UnplayedStoryLineProbability;
            float playedProbability = storyLineSettings.PlayedStoryLineProbability;

            StoryLines availableStoryLines = storyLineSettings.AvailableStoryLines;
            StoryLines completedStoryLines = storyLineSettings.CompletedStoryLines;
            
            PlotProbabilities probabilities = new PlotProbabilities()
            {
                Ben = availableStoryLines.Ben ? completedStoryLines.Ben ? unplayedProbability : playedProbability : 0f,
                Alex = availableStoryLines.Alex ? completedStoryLines.Alex ? unplayedProbability : playedProbability : 0f,
                Alyssa = availableStoryLines.Alyssa ? completedStoryLines.Alyssa ? unplayedProbability : playedProbability : 0f,
                Jake = availableStoryLines.Jake ? completedStoryLines.Jake ? unplayedProbability : playedProbability : 0f,
                Julie = availableStoryLines.Julie ? completedStoryLines.Julie ? unplayedProbability : playedProbability : 0f,
                Ryan = availableStoryLines.Ryan ? completedStoryLines.Ryan ? unplayedProbability : playedProbability : 0f
            };

            probabilities.Normalize();

            save.StoryLineData.Generate(probabilities);

            // TODO
            //save.ClueInventory.Clear();
        }
    }
}