﻿#if UNITY_EDITOR
using TheDayIDied.Mono.HouseSystems;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace TheDayIDied.Tools.Editors
{
    [CustomEditor(typeof(House), false)]
    public class HouseEditor : Editor
    {
        
        private House _house;

        private void OnEnable()
        {
            _house = (House)target;
        }
        
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            
            GUILayout.Label($"Rooms: {_house.Rooms.Count}");
            GUILayout.Space(15f);

            if (GUILayout.Button("Create Room"))
            {
                _house.CreateRoom();
            }
            
        }
        
    }
}
#endif