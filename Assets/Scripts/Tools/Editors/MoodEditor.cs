﻿#if UNITY_EDITOR

using TheDayIDied.Mono.NPC;
using UnityEditor;
using UnityEngine;

namespace TheDayIDied.Tools.Editors
{
    [CustomEditor(typeof(Mood), true)]
    public class MoodEditor : Editor
    {
        private Mood _mood;

        private void OnEnable()
        {
            _mood = (Mood)target;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            
            GUILayout.Label($"Current Mood: {_mood.CurrentEmotion}");
            GUILayout.Space(15f);

            if (GUILayout.Button("Angry"))
            {
                _mood.CurrentEmotion = Emotion.Angry;
            }
            if (GUILayout.Button("Overwhelmed"))
            {
                _mood.CurrentEmotion = Emotion.Overwhelmed;
            }
            if (GUILayout.Button("Nervous"))
            {
                _mood.CurrentEmotion = Emotion.Nervous;
            } 
            if (GUILayout.Button("Calm"))
            {
                _mood.CurrentEmotion = Emotion.Calm;
            }
            
            GUILayout.Space(15f);
            
            if (GUILayout.Button("Toggle Play"))
            {
                _mood.ToggleMood();
            }
            
        }
    }
}

#endif