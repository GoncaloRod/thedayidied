﻿#if UNITY_EDITOR
using TheDayIDied.Mono.Interactables.Electronics;
using UnityEditor;
using UnityEngine;

namespace TheDayIDied.Tools.Editors
{
    [CustomEditor(typeof(ElectronicDevice), true)]
    public class ElectronicDeviceEditor : Editor
    {
        private ElectronicDevice _electronicDevice;

        private void OnEnable()
        {
            _electronicDevice = (ElectronicDevice)target;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            
            GUILayout.Space(15f);

            if (GUILayout.Button("Toggle Power"))
            {
                _electronicDevice.TogglePower();
            }
        }
    }
}
#endif
