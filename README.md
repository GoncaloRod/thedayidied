# The Day I Died

- [Requirements](#Requirements)
- [Project Structure](#Project-Structure)

## Requirements

- Unity 2020.3.22f1 LTS

## Project Structure

```
|- Input                                InputSystem action files
|- Renderer                             Renderer related files
|- Resources                            Every game asset (3D models, fonts, UI, ...)
    |- Cool house
        |- Source Files                 Source files for this resource (.blend, .spp, .psd, ...)
        |- Models                       Exported models (.fbx, .obj, ...)
        |- Textures                     Exported textures (.png, ...)
        |- Materials                    Unity material files
        CoolHouse.prefab                Unity prefab for this asset
    |- Nice car
    |- Weird lamp
    |- ...
|- Scenes                               Every scene and scene templates
    |- Templates                        Template scenes (May or may not be used)
    |- VeryNiceScene.scene
    |- ...
|- ScriptableObjects                    Every ScriptableObject
    |- Clue 1
    |- Clue 2
    |- ...
|- Source
    |- Input                            InputSystem generated classes
    |- Mono                             Every MonoBehaviour script
    |- ScriptableObjects                Every ScriptableObject classes
    |- Tools                            Custom editors, automations, ...
|- Vendor                               Third-party libraries (tipicly from the Asset Store)
    |- Shapes
    |- ...
```